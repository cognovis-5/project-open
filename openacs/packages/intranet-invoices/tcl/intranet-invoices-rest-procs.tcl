# /packages/intranet-sencha-tables-rest-procs

ad_library {
	Rest Procedures for the sencha-tables package
	
	@author malte.sussdorff@cognovis.de
}

ad_proc -public im_rest_get_custom_invoice_email_texts {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for getting email texts
} {
	array set query_hash $query_hash_pairs
    
    # Try to get the locale
    if {[info exists query_hash(locale)]} {
	    set current_locale $query_hash(locale)
    } else {
	    set current_locale [lang::user::locale -user_id $rest_user_id] 
    }
    
    set default_locale en_US
	set default_locale_label [lang::util::get_label $default_locale]
	set package_key "intranet-invoices"
	
	set obj_ctr 0
	set valid_vars [list category translated_subject translated_body default_subject default_body default_subject_edit_url default_body_edit_url translated_subject_edit_url translated_body_edit_url]
    set result ""
    
    db_foreach category "select parent_id, category_id from im_categories left outer join im_category_hierarchy on (child_id = category_id) where category_type = 'Intranet Cost Type' and enabled_p = 't' order by im_name_from_id(category_id)" {
    
    	# get the data used in mails
    	if {[db_0or1row invoice_info "select *, company_contact_id as recipient_id from im_costs, im_invoices where cost_id = invoice_id and cost_id = (select max(cost_id) from im_costs where cost_type_id = :category_id)"]} {
    	db_1row user_info "select first_names, last_name from persons where person_id = :recipient_id"
    
	    	set recipient_locale [lang::user::locale -user_id $recipient_id]
	    
	    	callback intranet-invoices::mail_before_send -invoice_id $invoice_id -type_id $cost_type_id
	    
	    	if {![db_0or1row related_projects_sql "
	    			select distinct
	    			r.object_id_one as project_id,
	    			p.project_name,
	    			project_lead_id,
	    			im_name_from_id(project_lead_id) as project_manager,
	    			p.project_nr,
	    			p.parent_id,
	    			p.description,
	    			trim(both p.company_project_nr) as customer_project_nr
	    		from
	    				acs_rels r,
	    			im_projects p
	    		where
	    			r.object_id_one = p.project_id
	    			and r.object_id_two = :invoice_id
	    			order by project_id desc
	    			limit 1
	    	"]} {
	    		set project_name ""
	    		set project_manager [im_name_from_user_id $user_id]
	    		set project_lead_id $user_id
	    		set customer_project_nr ""
	    		set project_nr ""
	    	}
    	}
    
    	if {"" != $parent_id} {
    		set parent "[im_category_from_id $parent_id] -- "
    	} else {
    		set parent ""
    	}
    
    
    	set category "$parent[im_category_from_id $category_id]"
    	
    	set default_subject [lang::message::lookup $default_locale intranet-invoices.invoice_email_subject_${category_id} "missing"]
   		set default_body [lang::message::lookup $default_locale intranet-invoices.invoice_email_body_${category_id} "missing"]
    	if {$default_subject eq ""} {
    		set translated_subject ""
    	} else {
    		set translated_subject [lang::message::lookup $current_locale intranet-invoices.invoice_email_subject_${category_id} "missing"]
    	}
    	if {$default_body eq ""} {
    		set translated_body ""
    	} else {
    		set translated_body [lang::message::lookup $current_locale intranet-invoices.invoice_email_body_${category_id} "missing"]
    	}

		foreach var [list default_subject default_body translated_subject translated_body] {
			if {[set $var] eq "missing"} {
				set $var ""
			}
		}
		# URLs
    	set default_subject_edit_url [export_vars -base "/acs-lang/admin/edit-localized-message" -url {{package_key "intranet-invoices"} {message_key "invoice_email_subject_${category_id}"} {locale $default_locale}}]
    	set default_body_edit_url [export_vars -base "/acs-lang/admin/edit-localized-message" -url {{package_key "intranet-invoices"} {message_key "invoice_email_body_${category_id}"} {locale $default_locale}}]
    	set translated_subject_edit_url [export_vars -base "/acs-lang/admin/edit-localized-message" -url {{package_key "intranet-invoices"} {message_key "invoice_email_subject_${category_id}"} {locale $current_locale}}]
    	set translated_body_edit_url [export_vars -base "/acs-lang/admin/edit-localized-message" -url {{package_key "intranet-invoices"} {message_key "invoice_email_body_${category_id}"} {locale $current_locale}}]
    
    	# Now create the json
	    set komma ",\n"
	    if {0 == $obj_ctr} { set komma "" }
	    set dereferenced_result ""
	    	    
	    foreach v $valid_vars {
	    		eval "set a $$v"
	    		regsub -all {\n} $a {\n} a
	    		regsub -all {\r} $a {} a
	    		append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
	    }
	    
	    append result "$komma{\"id\": \"$category_id\", \"object_name\": \"$category\"$dereferenced_result}"
	    incr obj_ctr
	    
    }
	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_freelance_skills: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}