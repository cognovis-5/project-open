#!/bin/bash
PKGS_LIST=(cognovis-core cognovis-rest intranet-invoices intranet-openoffice intranet-material intranet-chilkat intranet-fs intranet-collmex)
PKGS_OLD_LIST=(intranet-freelance intranet-freelance-invoices)
DEPRECATE_LIST=(views intranet-jquery intranet-mail)

for pkg in ${PKGS_LIST[@]} ; do echo $pkg \
    && rm -rf $pkg && curl -o $pkg.tar.gz https://gitlab.com/cognovis-5/$pkg/-/archive/master/$pkg.tar.gz \
    && tar xfz $pkg.tar.gz && mv ${pkg}-master-* $pkg && rm $pkg.tar.gz ; done

for pkg in ${PKGS_OLD_LIST[@]} ; do echo $pkg \
    && rm -rf $pkg && curl -o $pkg.tar.gz -q https://gitlab.com/cognovis/$pkg/-/archive/master/$pkg.tar.gz \
    && tar xfz $pkg.tar.gz && mv ${pkg}-master-* $pkg && rm $pkg.tar.gz ; done

for pkg in ${DEPRECATE_LIST[@]} ; do echo $pkg \
    && rm -rf $pkg && curl -o $pkg.tar.gz -q https://gitlab.com/cognovis/$pkg/-/archive/master/$pkg.tar.gz \
    && tar xfz $pkg.tar.gz && mv ${pkg}-master-* $pkg && rm $pkg.tar.gz ; done