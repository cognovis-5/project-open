ad_page_contract {
    Display information about one procedure.
    
    @cvs-id $Id$
} {
    proc_name
    {source_p 0}
}

if {![info exists format]} { set format "html" }

set title $proc_name

set context [list]
lappend context [list $proc_name]

set return_url ""
set documentation "[im_rest_proc_documentation -proc_name $proc_name -source_p $source_p]"

switch $format {
    json {}
    default {}
}
