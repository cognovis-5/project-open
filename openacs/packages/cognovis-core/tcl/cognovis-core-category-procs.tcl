ad_library {
    Rest Procedures for the cognovis-core package catgories
    @author malte.sussdorff@cognovis.de
}

#---------------------------------------------------------------
# Categories
#---------------------------------------------------------------

ad_proc -public im_category_all_parents {
    -category_id
} {
    set all_parent_ids [list]
    set parent_ids [im_category_parents $category_id]
    if {$parent_ids ne ""} {
        foreach parent_id $parent_ids {
            lappend all_parent_ids $parent_id
            set all_parent_ids [concat $all_parent_ids [im_category_all_parents -category_id $parent_id]]
        }
    }
    return $all_parent_ids
}

ad_proc -public im_category_string1 {
    -category_id
    {-locale "" }
} {
    Return the localized version of string1 for the category
} {
    set category_key "intranet-core.string1_$category_id"
    set default_string1 [db_string string1 "select aux_string1 from im_categories where category_id = :category_id" -default ""]
    if {$default_string1 eq ""} {
        return ""
    }
    return [lang::message::lookup $locale $category_key $default_string1]
}

ad_proc -public im_category_string2 {
    -category_id
    {-locale "" }
} {
    Return the localized version of string1 for the category
} {
    set category_key "intranet-core.string2_$category_id"
    set default_string2 [db_string string2 "select aux_string2 from im_categories where category_id = :category_id" -default ""]
    return [lang::message::lookup $locale $category_key $default_string2]
}

ad_proc -public im_category_icon_or_color {
    -category_id
} {
    Returns the icon for the category_type or the color if it is a category_status

    Go up the category hierarchy until you find one or there is no parent

    @returns aux_html2 of the category in question (or the parents)
} {
    set aux_html2 [db_string type_from_category "select aux_html2 from im_categories where category_id = :category_id" -default ""]
    if {$aux_html2 eq ""} {
        set parent_ids [im_category_parents $category_id]
        foreach parent_id $parent_ids {
            if {$aux_html2 eq ""} {
                if {$parent_id ne $category_id} {
                    set aux_html2 [im_category_icon_or_color -category_id $parent_id]
                }
            }
        }
    }

    return $aux_html2
}

ad_proc -public im_category_visible_p {
    -category_id
    {-user_id ""}
} {
    Return 1 if the category_id is visible to the user taking visible_tcl into account
} {
    if {"" == $user_id} {set user_id [ad_conn user_id]}
    set visible_tcl [db_string visible_tcl "select visible_tcl from im_categories where category_id = :category_id" -default ""]
    if {"" == $visible_tcl} {
        return 1
    } else {
        set visible 0
        set errmsg ""
        catch {     set visible [eval $visible_tcl] }
        return $visible
    }
}

ad_proc -public im_category_type_reorder {
        -category_type
        -table_columns
        -limit_to_top
} {
        Resort the sort_order based on the number of times a category was used in the table_columns

        @param category_type Category Type to reorder
        @param limit_to_top Limit this to the top x entries. All others will have the sort_order unset
        @param table_columns List of "table_name" "column_name" pairs to use for counting
} {
        set category_ids [db_list categories "select category_id from im_categories where category_type = :category_type"]

        template::multirow create sort_orders category_id counter
        foreach category_id $category_ids {
                set counter 0
                foreach table_column $table_columns {
                        set table_name [lindex $table_column 0]
                        set column_name [lindex $table_column 1]
                        set num_used [db_string count "select count(*) from $table_name where $column_name = :category_id" -default 0]
                        set counter [expr $counter + $num_used]
                }
                template::multirow append sort_orders $category_id $counter
        }

        # ---------------------------------------------------------------
        # Write the new sort orders
        # ---------------------------------------------------------------
        template::multirow sort sort_orders -integer -decreasing counter
        set sort_order 1
        template::multirow foreach sort_orders {
                if {$sort_order <= $limit_to_top} {
                        db_dml update_sort_order "update im_categories set sort_order = :sort_order where category_id = :category_id"
                        ds_comment "$sort_order :: [im_category_from_id $category_id]:: $counter"
                } else {
                        db_dml unser_sort_order "update im_categories set sort_order = null where category_id = :category_id"
                        ds_comment "Unset sort_order for [im_category_from_id $category_id]"
                }
                incr sort_order
        }
}

ad_proc -public cog_category_id {
    -category:required
    -category_type:required
} {
    Returns the category_id for a given category of a Type

    @param category Category (not translated) to use for lookup
    @param categroy_type Limit to only category types of this types

    @see im_category_from_category

    @return category_id Category ID
} {

    if {$category eq ""} {return ""}
    return [db_string cat "select category_id from im_categories where category = :category and category_type = :category_type" -default ""]
} 

ad_proc -public cog_sub_categories {
    {-include_disabled_p 1}
    {-exclude_self_p 0}
    category_list
} {
    Takes a single category or a list of categories and
    returns a list of the transitive closure (all sub-
    categories).
} {
    # Add a dummy value so that an empty input list doesn't
    # give a syntax error...
    lappend category_list 0
    
    # Check security. category_list should only contain integers.
    im_security_alert_check_integer \
	-location "im_sub_categories" \
	-value $category_list

    # Should we include disabled categories? This is necessary for
    # example if we want to disable all sub-categories of a top category
    set enabled_check "and (c.enabled_p = 't' OR c.enabled_p is NULL)"
    if {$include_disabled_p} { set enabled_check "" }

    # Should we include original categories
    if {$exclude_self_p} {
        set exclude_self_check "where category_id not in ([join $category_list ","])"
    } else { 
        set exclude_self_check "" 
    }

    set closure_sql "
    select category_id from (
        select	category_id, sort_order
        from	im_categories c
        where	c.category_id in ([join $category_list ","])
            $enabled_check
        UNION
        select	h.child_id, sort_order
        from	im_categories c,
            im_category_hierarchy h
        where	h.parent_id in ([join $category_list ","])
            and h.child_id = c.category_id
            $enabled_check
    ) cats $exclude_self_check order by sort_order
    "

    set result [util_memoize [list db_list category_trans_closure $closure_sql]]

    # Avoid SQL syntax error when the result is used in a where x in (...) clause
    if {"" == $result} { set result [list 0] }

    return $result
}