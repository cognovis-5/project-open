ad_library {
    Procedures handling absences, taken from intranet-timesheet changes
    @author malte.sussdorff@cognovis.de
}

ad_proc -public im_user_absence_status_planned {} {
    Planned status
} {
    return 16009
}
ad_proc -public im_user_absence_status_requested_or_active {} { return 16001 }

ad_proc -private im_absence_component_view_p {
    -user_selection:required 
} {

    Returns true if the current user (current_user_id) can view the 
    absence component (Absence Cube, Absence Calendar) of another 
    user (owner_id).

    Only allows this for the owner, for users with HR permissions,
    supervisor of the employee, and the cost center (department) manager.

    @author Neophytos Demetriou (neophytos@azet.sk)
} {

    set current_user_id [auth::get_user_id]

    return \
        [im_absence_component__user_selection_helper \
            -user_selection $user_selection \
            -user_selection_idVar user_selection_id \
            -user_selection_typeVar user_selection_type]

}

ad_proc -public im_absence_user_component {
    -user_id:required
} {
    Returns a HTML component showing the vacations
    for the user
} {
    if { ![im_absence_component_view_p -user_selection $user_id] } {
#        return "You do not have enough privileges to view this component"
	return "" ; # Returning nothing will hide the component. Horray!
    }

    set params [list \
		    [list user_id_from_search $user_id] \
		    [list return_url [im_url_with_query]] \
    ]

    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/user-absences"]
    return [string trim $result]
}

ad_proc -public im_absence_info_component {
    -absence_id:required
} {
    Returns a HTML component showing the info about an absence
} {
    set current_user_id [auth::get_user_id]


    set params [list \
		    [list absence_id $absence_id] \
		    [list return_url [im_url_with_query]] \
    ]

    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/absence-info"]
    return [string trim $result]
}

ad_proc -public im_absence_balance_component {
    -user_id:required
} {
    Returns a HTML component showing the absence balance for a user
} {

    set params [list \
		    [list user_id $user_id] \
		    [list return_url [im_url_with_query]] \
    ]

    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/absence-balance-component"]
    return [string trim $result]
}

ad_proc -public im_absence_remaining_days {
    -user_id:required
    -absence_type_id:required
    -approved:boolean
    {-ignore_absence_ids ""}
    {-booking_date ""}
    {-include_planned_p "0"}
} {
    Returns the number of remaining days for the user of a certain absence type
    @param ignore_absence_id Ignore this absence_id when calculating the remaining days.
    @param booking_date Parameter to be used for leave entitlements to define which leave entitlements should be included. Defaults to current date (everything earned up until today)
} {
    if {[im_table_exists im_user_leave_entitlements]} {
	    return [im_leave_entitlement_remaining_days -user_id $user_id -absence_type_id $absence_type_id -approved_p $approved_p -ignore_absence_ids $ignore_absence_ids -booking_date $booking_date -include_planned_p $include_planned_p]
        ad_script_abort
    }
    
    set current_year [db_string current_year "select to_char(now(), 'YYYY')"]

    set start_of_year "$current_year-01-01"
    set end_of_year "$current_year-12-31"

    if {!$approved_p} {
        set approved_sql ""
    } else {
        set approved_sql ""
    }

    db_1row user_info "select coalesce(vacation_balance,0) as vacation_balance,
                          coalesce(vacation_days_per_year,0) as vacation_days_per_year,
                          coalesce(overtime_balance,0) as overtime_balance,
                          coalesce(rwh_days_last_year,0) as rwh_days_last_year,
                          coalesce(rwh_days_per_year,0) as rwh_days_per_year
                      from im_employees where employee_id = :user_id"
    switch $absence_type_id {
    	5000 {
                # Vacation
    	    set entitlement_days [expr $vacation_balance + $vacation_days_per_year]
    	}
    	5006 {
                # Overtime
                set entitlement_days $overtime_balance
            }
    	5007 {
                # RTT
    	    set entitlement_days [expr $rwh_days_last_year + $rwh_days_per_year]
    	}
    	default {
                set entitlement_days 0
    	}
    }
    
    # Include planned days when checking for absences
    if {$include_planned_p} {
        lappend absence_type_id [im_user_absence_status_planned]
    }
    
    set absence_days [im_absence_days -owner_id $user_id -absence_type_ids $absence_type_id -start_date $start_of_year -end_date $end_of_year -approved_p $approved_p -ignore_absence_ids $ignore_absence_ids]
    
    set remaining_days [expr $entitlement_days - $absence_days]
    return $remaining_days
}

ad_proc -public im_absence_days {
    {-owner_id ""}
    {-group_ids ""}
    -absence_type_ids:required
    {-approved_p "0"}
    {-start_date ""}
    {-end_date ""}
    {-ignore_absence_ids ""}
} {
    Returns the number of absence days for the user or group of a certain absence type
    @param ignore_absence_id Ignore this absence_id when calculating the remaining days.
} {

    return [im_absence_dates -owner_id $owner_id -group_ids $group_ids -start_date $start_date -end_date $end_date -absence_type_ids $absence_type_ids -ignore_absence_ids $ignore_absence_ids -type "sum"]
}


#### Procedure to calculate the vacation days in a given time period, judged from the start and end dates

ad_proc -public im_absence_week_days {
    -start_date:required
    -end_date:required
    {-week_day_list {0 6}}
    {-type "dates"}
} {
    Given a list of week_days, return the actual dates for those week_days
    
    @param start_date Start of the interval
    @param end_date End of the interval
    @param week_day_list List of weekdays, where 0 = Sunday and 6 = Saturday. Defaults to the weekend (Saturday and Sunday, 6 & 0)
} {
    return [util_memoize [list im_absence_week_days_helper -start_date $start_date -end_date $end_date -week_day_list $week_day_list -type $type]]
}

ad_proc -public im_absence_week_days_helper {
    -start_date:required
    -end_date:required
    {-week_day_list {0 6}}
    {-type "dates"}
} {
    Given a list of week_days, return the actual dates for those week_days
    
    @param start_date Start of the interval
    @param end_date End of the interval
    @param week_day_list List of weekdays, where 0 = Sunday and 6 = Saturday. Defaults to the weekend (Saturday and Sunday, 6 & 0)
} {
    # Now substract the off days
    set week_day_clause_list [list]
    foreach week_day $week_day_list {
        lappend week_day_clause_list "extract('dow' FROM i)=$week_day" 
    }
    
    if {$week_day_list eq ""} { set where_clause ""} else { set where_clause "WHERE [join $week_day_clause_list " or "]"}

        set dates [db_list date_range "
            SELECT to_char(i,'YYYY-MM-DD')
            FROM (
              SELECT generate_series(start, finish, '1 day') AS i
              FROM
                  (VALUES(
                  '$start_date'::date,
                  '$end_date'::date
              )) AS t(\"start\", \"finish\")
            ) AS j
            $where_clause
        "]
    if {$type == "dates"} {
        return [lsort $dates]
    } else {
        return [llength $dates]
    }
}

#### Procedure to calculate the days to take given a start and end date

ad_proc -public im_absence_dates {
    {-owner_id ""}
    {-group_ids ""}
    {-start_date ""}
    {-end_date ""}
    {-exclude_week_days {0 6}}
    {-absence_type_ids ""}
    {-absence_status_id ""}
    {-ignore_absence_ids ""}
    {-type "dates"}
} {
    Returns a list of dates in an interval, where an owner or a group is not working.
    
    @param owner_id Owner for whom we calculate the actual absence
    @param group_ids Alternatively calculate for this group_ids. If neither group_ids nor owner_id is provided return absences for any group
    @param start_date Start of the absence
    @param end_date End of the absence
    @param off_days Days which we do not count in our calculation. That is usually Saturday and Sunday (weekends), but might be other dates as well
    @param include_personal Should we include personal vacations in the calculation? Usually we don't, as they are e.g. sickness
    @param ignore_absence_id Ignore this absence_id when calculating the dates. This is helpful if we edit an existing absence and want to get the other days the user is off
    @param type "dates" which is default returns the dates. "sum" returns the actual amount of days and "absence_ids" lists the absences which fall in this timeframe
} {
    # Assume current year for start/enddate
    set current_year [dt_systime -format "%Y"]
    if {$start_date eq ""} {
        set start_date "${current_year}-01-01"
    }
    if {$end_date eq ""} {
	    set end_date "${current_year}-12-31"
    }

    # If we have an owner_id limit the absences to only this owner and the group the owner belongs to
    
    if {$owner_id ne ""} {
        # Get the groups the owner belongs to
        set group_ids [im_biz_object_memberships -member_id $owner_id]

         # Add registered_users
         lappend group_ids "-2"

         set owner_sql "and (owner_id = :owner_id or group_id in ([template::util::tcl_to_sql_list $group_ids]))"
    } elseif {$group_ids ne ""} {
        # We try to find the holidays for the group of users
        set owner_sql "and group_id in ([template::util::tcl_to_sql_list $group_ids])"
    } else {
        # We try to find the holidays for any group
        set owner_sql "and group_id is not null"
    }
    

    # We need to ignore this absence_id from the calculation of
    # absence days. Usually during an edit
    if {$ignore_absence_ids eq ""} {
	    set ignore_absence_sql ""
    } else {
        set ignore_absence_sql "and absence_id not in ([template::util::tcl_to_sql_list $ignore_absence_ids])"
    }
    
    # Check for the absence types
    if {""==$absence_type_ids} {
        set absence_type_sql ""
    } else {
        set absence_type_sql "and absence_type_id in ([template::util::tcl_to_sql_list $absence_type_ids])"
    }
    
    if {"" == $absence_status_id} {
        set absence_status_sql "absence_status_id != [im_user_absence_status_deleted]"
    } else {
        set absence_status_sql "absence_status_id in ([template::util::tcl_to_sql_list [im_sub_categories $absence_status_id]])"
    } 

    set absence_days [list]
    set absence_ids [list]

    #   Define the days of the week we look into
    set days_of_week [list 0 1 2 3 4 5 6]
    
    if {$exclude_week_days ne ""} {
        foreach exclude_day $exclude_week_days {
            set days_of_week  [lsearch -inline -all -not -exact $days_of_week $exclude_day]
        }
    }    

    # Now we need to find the absences which already exist in this timeframe and extract the dates it is occurring 
    db_foreach absence_ids "select absence_id, to_char(start_date,'YYYY-MM-DD') as absence_start_date, to_char(end_date,'YYYY-MM-DD') as absence_end_date
                from    im_user_absences 
                where   (start_date::date <= :end_date and
                        end_date::date >= :start_date and
                        $absence_status_sql
                        $absence_type_sql
                        $owner_sql)
                        $ignore_absence_sql
                        " {
        # Get the days for this absence based on the start and end_date
        lappend absence_ids $absence_id
        set absence_days [concat $absence_days [im_absence_week_days -week_day_list $days_of_week -start_date $absence_start_date -end_date $absence_end_date]]
    }
     
    # Remove duplicates
    set absence_days [lsort -unique $absence_days]
    
    # Absence Days now contains all the dates which he has already off
    if {$type == "dates"} {
        return [lsort $absence_days]
    } elseif {$type == "sum"} {
        return [llength $absence_days]
    } elseif {$type == "absence_ids"} {
        return [lsort $absence_ids]
    }
}

ad_proc -public im_absence_wf_exists_p {
    {-absence_type_id:required}
} {
    # Check if a workflow exists for this type
} {
    return [util_memoize [list im_absence_wf_exists_p_helper -absence_type_id $absence_type_id] 120]
}

ad_proc -public im_absence_wf_exists_p_helper {
    {-absence_type_id:required}
} {
    # Check if a workflow exists for this type
} {
    set wf_key [db_string wf "select trim(aux_string1) from im_categories where category_id = :absence_type_id" -default ""]
    set wf_exists_p [db_string wf_exists "select count(*) from wf_workflows where workflow_key = :wf_key"]
    return $wf_exists_p
}

ad_proc -public im_absence_calculate_absence_days {
    {-owner_id ""}
    {-group_ids ""}
    {-start_date ""}
    {-end_date ""}
    {-ignore_absence_ids ""}
    {-absence_id ""}
    {-exclude_week_days {0 6}}
    {-type "duration"}
    {-absence_type_id ""}
    {-substract_absence_type_ids ""}
} {
    Calculate the needed dates for an absence
    @param owner_id Owner for whom we calculate the actual absence
    @param group_ids Alternatively calculate for this group_ids. If neither group_ids nor owner_id is provided return absences for any group
    @param start_date Start of the absence
    @param end_date End of the absence
    @param ignore_absence_ids Ignore these absence_ids when calculating the dates. 
    @param absence_id In case we only want to look at one absence_id, then we need to make sure to calculate the duration correctly. It will be appended to the ignore_absence_ids for calculation
    @param type duration will return the sum of the days needed, dates will return the actual dates
} {
       
    set absence_status_id ""
    
    # does the absence exist?
    if {![db_string absence_exists_p "select 1 from im_user_absences where absence_id = :absence_id" -default 0]} {
        set absence_id ""
    }
    
    
    # Check if we calculate the days for an existing absence
    if {$absence_id ne ""} {
        lappend ignore_absence_ids $absence_id
        db_1row absence_data "select absence_type_id, absence_status_id, owner_id, group_id from im_user_absences where absence_id = :absence_id"
        
        # If we have an owner_id limit the absences to only this owner and the group the owner belongs to
        if {$owner_id ne ""} {
            # Get the groups the owner belongs to
            set group_ids [im_biz_object_memberships -member_id $owner_id]

             # Add registered_users
             lappend group_ids "-2"

             set owner_sql "and (owner_id = :owner_id or group_id in ([template::util::tcl_to_sql_list $group_ids]))"
        } elseif {$group_ids ne ""} {
            # We try to find the holidays for the group of users
            set owner_sql "and group_id in ([template::util::tcl_to_sql_list $group_ids])"
        } else {
            # We try to find the holidays for any group
            set owner_sql "and group_id is not null"
        }
        
        # Check if we have a workflow
        set wf_exists_p [im_absence_wf_exists_p -absence_type_id $absence_type_id]
        if {!$wf_exists_p} {
            set absence_status_sql "and absence_status_id not in ([template::util::tcl_to_sql_list [im_sub_categories [im_user_absence_status_active]]])"
        } else {
            set absence_status_sql "and absence_status_id != [im_user_absence_status_deleted]"
        } 
        
        # Additionally ignore any absence which has an older creation date and is of the same type.
        db_foreach ignoreable_absences "select object_id from acs_objects o, im_user_absences ua
            where o.object_id = ua.absence_id
            and ua.absence_type_id = :absence_type_id
            and ua.absence_status_id = :absence_status_id
            and o.creation_date > (select creation_date from acs_objects where object_id = :absence_id)
            and absence_id not in ([template::util::tcl_to_sql_list $ignore_absence_ids])
            $absence_status_sql
            $owner_sql
        " {
            lappend ignore_absence_ids $object_id
        }
                
        db_1row absence "select owner_id, duration_days from im_user_absences where absence_id = :absence_id"
        if {$end_date eq "" && $start_date eq ""} {
            db_1row absence "select start_date, end_date from im_user_absences where absence_id = :absence_id"
        }
    }
    
    # Get a list of dates in the range
    set dates_in_range [im_absence_week_days -week_day_list [list 0 1 2 3 4 5 6] -start_date $start_date -end_date $end_date]

    # Get the list of dates which are excluded
    if {$exclude_week_days eq ""} {
        set off_dates [list]
    } else {
        set off_dates [im_absence_week_days -week_day_list $exclude_week_days -start_date $start_date -end_date $end_date]
    }

    # If we have an ignore_absence_type_id then ignore the type
    if {$absence_type_id eq ""} {
        set absence_type_ids ""
    } else {
        set absence_type_ids [db_list higher_prio "select category_id from im_categories where category_type = 'Intranet Absence Type' and sort_order > (select sort_order from im_categories where category_id = :absence_type_id)"]
        lappend absence_type_ids $absence_type_id
    }
    
    
    # Get the existing absence dates in the interval for any higher category
    set existing_absence_dates [im_absence_dates -owner_id $owner_id -group_ids $group_ids -start_date $start_date -end_date $end_date -ignore_absence_ids $ignore_absence_ids -exclude_week_days $exclude_week_days -absence_type_ids $absence_type_ids -absence_status_id [im_user_absence_status_active]]

    # Join the dates together
    set existing_absence_dates [concat $existing_absence_dates $off_dates]

    # If this is a requested absence, append the requested days as existing absences as well
    if {[lsearch $absence_status_id [im_sub_categories [im_user_absence_status_requested]]]>-1} {
        set requested_absence_dates [im_absence_dates -owner_id $owner_id -group_ids $group_ids -start_date $start_date -end_date $end_date -ignore_absence_ids $ignore_absence_ids -exclude_week_days $exclude_week_days -absence_type_ids $absence_type_ids -absence_status_id [im_user_absence_status_requested]]
        set existing_absence_dates [concat $existing_absence_dates $requested_absence_dates]        
    }
    
    # Now check for each date in the range whether we need to take vacation then
    set required_dates [list]
    foreach date $dates_in_range {
        if {[lsearch $existing_absence_dates $date]<0} {
            lappend required_dates $date
        }
    }
    
    # Update the duration in the database for compatability reasons
    set new_duration [llength $required_dates]
    if {$absence_id ne ""} {
        # Check if duration changed
        if {[expr $new_duration - $duration_days] != 0} {
            # Update the duration in the database
            db_dml update_duration "update im_user_absences set duration_days = :new_duration where absence_id = :absence_id"
        }
    }
    
    if {$type == "duration"} {
        return [llength $required_dates]
    } else {
        return [lsort $required_dates]
    }
}

ad_proc -public im_absence_update_duration_days {
    {-interval "2 months"}
} {
    set absence_ids [db_list absences "select absence_id from im_user_absences,acs_objects where absence_id = object_id and creation_date > now() - interval :interval"]
    foreach absence_id $absence_ids {
        set duration_days [im_absence_calculate_absence_days -absence_id $absence_id]
        cog_log Debug "Updateing Absence $absence_id to $duration_days"
    }
}

ad_proc -public im_absence_approval_component {
    -user_id:required
} {
    Returns a HTML component showing the vacations
    for the user
} {
    set current_user_id [auth::get_user_id]
    # This is a sensitive field, so only allows this for the user himself
    # and for users with HR permissions.

    set read_p 0
    if {$user_id == $current_user_id} { set read_p 1 }
    if {[im_permission $current_user_id view_hr]} { set read_p 1 }
    if {!$read_p} { return "" }

    set params [list \
		    [list user_id $user_id] \
		    [list return_url [im_url_with_query]] \
    ]

    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/absence-approval"]
    return [string trim $result]
}

ad_proc -public im_absence_types {} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {
    set color_list [im_absence_cube_color_list]
    set sql "
        select category_id, category, enabled_p, aux_string2
        from im_categories
        where category_type = 'Intranet Absence Type'
        order by category_id
    "
    set result [list]
    set index -1
    db_foreach absence_category $sql {
        if { $aux_string2 eq {} } {
            set bg_color [lindex $color_list $index]
            incr index
        } else {
            set bg_color $aux_string2
        }
        set fg_color [im_absence_fg_color $bg_color]
        lappend result [list $category_id $category $enabled_p $bg_color $fg_color]
    }
    return $result
}


ad_proc -public im_absence_cube_legend {} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {

    append admin_html "<div class=filter-title>[lang::message::lookup "" intranet-timesheet2.Color_codes "Color Codes"]</div>\n"
    append admin_html "<table cellpadding='5' cellspacing='5'>\n"

    # Marc Fleischer: A question of color
    set index -1
    set categories [im_absence_types]
    foreach category_item $categories {
        foreach {category_id category enabled_p bg_color fg_color} $category_item break

        if { "t" == $enabled_p } {
            regsub -all { } $category {_} category_key
            set category_l10n [lang::message::lookup "" intranet-core.$category_key $category]
            append admin_html "<tr>[im_absence_render_cell $bg_color $fg_color $category_l10n left]</tr>\n"
       }
    }

    append admin_html "</table>\n"
}

ad_proc -public im_absence_ics {
    {-absence_id:required}
} {
    the cal_item_id is obvious.
} {
    db_1row absence_info "select description, absence_name, owner_id, to_char(o.creation_date,'YYYY-MM-DD HH24:MI:SS') as creation_date,absence_status_id, absence_type_id, to_char(start_date,'YYYYMMDD') as start_date, to_char(end_date + interval '1 day','YYYYMMDD') as end_date, to_char(o.last_modified,'YYYY-MM-DD HH24:MI:SS') as last_modified from im_user_absences ua, acs_objects o where absence_id = :absence_id and ua.absence_id = o.object_id"

    # Get some defaults
    set owner_name [im_name_from_user_id $owner_id]
    set owner_email [party::email -party_id $owner_id]
    set CREATED [calendar::outlook::ics_timestamp_format -timestamp $creation_date]
    set DTSTAMP [calendar::outlook::ics_timestamp_format -timestamp $last_modified]
    set DESCRIPTION $absence_name
    set SUMMARY "[im_category_from_id $absence_type_id] [im_category_from_id $absence_status_id] ($owner_name)"
    set UID $absence_id
    
    if {[lsearch [im_sub_categories [im_user_absence_status_requested]] $absence_status_id]>=0} {
        set METHOD "PUBLISH"
        set SEQUENCE "0"
        set TRANSP "TRANSPARENT"
        set STATUS "TENTATIVE"
        set BUSYSTATUS "FREE"
    } elseif {[lsearch [im_sub_categories [im_user_absence_status_active]] $absence_status_id]>=0} {
        set METHOD "PUBLISH"        
        set BUSYSTATUS "OOF"
        set TRANSP "OPAQUE"
        set STATUS "CONFIRMED"
        set SEQUENCE "1"
    } elseif {[lsearch [im_sub_categories [im_user_absence_status_deleted]] $absence_status_id]>=0} {
        set METHOD "CANCEL"
        set SEQUENCE "2"
        set TRANSP "TRANSPARENT"
        set STATUS "CANCELLED"
        set BUSYSTATUS "OOF"
    } else {
        # Default to active
        set METHOD "PUBLISH"        
        set BUSYSTATUS "OOF"
        set TRANSP "OPAQUE"
        set STATUS "CONFIRMED"
        set SEQUENCE "1"
    } 
    
    # Put it together
    set ics_event "BEGIN:VCALENDAR\r\nPRODID:-//OpenACS//OpenACS 5.0 MIMEDIR//EN\r\nVERSION:2.0\r\nMETHOD:$METHOD\r\n"
    
    # Now for the actual event
    append ics_event "BEGIN:VEVENT\r\nCREATED:$CREATED\r\nDTSTART;VALUE=DATE:$start_date\r\nDTEND;VALUE=DATE:$end_date\r\nLOCATION:\r\nX-MICROSOFT-CDO-BUSYSTATUS:$BUSYSTATUS\r\nTRANSP:$TRANSP\r\nSEQUENCE:$SEQUENCE\r\nUID:$UID\r\nDTSTAMP:$DTSTAMP\r\nDESCRIPTION:$DESCRIPTION\r\nSUMMARY:$SUMMARY\r\nSTATUS:$STATUS\r\nPRIORITY:5\r\nCLASS:PUBLIC\r\n"
    
    append ics_event "END:VEVENT\r\nEND:VCALENDAR\r\n"
    return $ics_event
}

ad_proc -public im_absence_vevent {
    {-absence_id:required}
    {-hide_type_p 0}
} {
    Returns the ICS for one VEVENT. Still needs the calendar wrapper in order to succeed
} {
    db_1row absence_info "select description, absence_name, owner_id, to_char(o.creation_date,'YYYY-MM-DD HH24:MI:SS') as creation_date,absence_status_id, absence_type_id, to_char(start_date,'YYYYMMDD') as start_date, to_char(end_date + interval '1 day','YYYYMMDD') as end_date, to_char(o.last_modified,'YYYY-MM-DD HH24:MI:SS') as last_modified from im_user_absences ua, acs_objects o where absence_id = :absence_id and ua.absence_id = o.object_id"

    # Get some defaults
    set owner_name [im_name_from_user_id $owner_id]
    set owner_email [party::email -party_id $owner_id]
    set CREATED [calendar::outlook::ics_timestamp_format -timestamp $creation_date]
    set DTSTAMP [calendar::outlook::ics_timestamp_format -timestamp $last_modified]
    set DESCRIPTION $absence_name
        
    if {$hide_type_p} {
        set SUMMARY "$owner_name ([im_category_from_id $absence_status_id])"
    } else {
        set SUMMARY "${owner_name}: [im_category_from_id $absence_type_id] ([im_category_from_id $absence_status_id])"
    }
    
    set UID $absence_id
    set ORGANIZER $owner_email

    if {[lsearch [im_sub_categories [im_user_absence_status_requested]] $absence_status_id]>=0} {
        set METHOD "PUBLISH"
        set SEQUENCE "0"
        set TRANSP "TRANSPARENT"
        set STATUS "TENTATIVE"
        set BUSYSTATUS "FREE"
    } elseif {[lsearch [im_sub_categories [im_user_absence_status_active]] $absence_status_id]>=0} {
        set METHOD "PUBLISH"        
        set BUSYSTATUS "OOF"
        set TRANSP "OPAQUE"
        set STATUS "CONFIRMED"
        set SEQUENCE "1"
    } elseif {[lsearch [im_sub_categories [im_user_absence_status_deleted]] $absence_status_id]>=0} {
        set METHOD "CANCEL"
        set SEQUENCE "2"
        set TRANSP "TRANSPARENT"
        set STATUS "CANCELLED"
        set BUSYSTATUS "OOF"
    }  else {
     # Default to active
     set METHOD "PUBLISH"        
     set BUSYSTATUS "OOF"
     set TRANSP "OPAQUE"
     set STATUS "CONFIRMED"
     set SEQUENCE "1"
    } 

    # Now for the actual event
    set vevent "\r\nBEGIN:VEVENT\r\nCREATED:$CREATED\r\nDTSTART;VALUE=DATE:$start_date\r\nDTEND;VALUE=DATE:$end_date\r\nLOCATION:\r\nX-MICROSOFT-CDO-BUSYSTATUS:$BUSYSTATUS\r\nTRANSP:$TRANSP\r\nSEQUENCE:$SEQUENCE\r\nUID:$UID\r\nDTSTAMP:$DTSTAMP\r\nDESCRIPTION:$DESCRIPTION\r\nSUMMARY:$SUMMARY\r\nSTATUS:$STATUS\r\nPRIORITY:5\r\nCLASS:PUBLIC\r\nEND:VEVENT"
    return $vevent
}

ad_proc -public im_absence_calculate_end_date {
    -start_date
    -interval_days
    {-increase_days ""}
    {-group_ids ""}
    {-level 0}
    {-ignore_absence_ids ""}
    {-exclude_week_days {0 6}}
} {
    Calculate the end date given the holidays and weekends
    
    @param start_date Date from which to start counting
    @param interval_days to add
    @param level used to avoid infinite loops
} {
    if {$increase_days eq ""} {set increase_days $interval_days}
    set end_date [db_string end_date "select to_char(to_timestamp(:start_date,'YYYY-MM-DD') + interval '$increase_days days','YYYY-MM-DD') from dual"]
    set day_diff [im_absence_calculate_absence_days \
        -group_ids $group_ids \
        -start_date $start_date \
        -end_date $end_date \
        -ignore_absence_ids $ignore_absence_ids \
        -exclude_week_days $exclude_week_days]

    set difference [expr $interval_days - $day_diff]

    if {$difference > 0 && $level <10} {
        # We have absences in the timeframe. So Increase the end date by the day_diff and find
        # Additional absences between the start_date and the new end_date
        set increase_days [expr $increase_days + $difference]
        incr level
        set end_date [im_absence_calculate_end_date -start_date $start_date -interval_days $interval_days -level $level -increase_days $increase_days]
    }
    return $end_date
}


ad_proc -private im_absence_component__absence_criteria {
    -where_clauseVar:required
    -user_selection:required
    {-absence_type_id ""}
    {-absence_status_id ""}
    {-current_user_id ""}
} {
    @param current_user_id Ability to pass in the current user id especially when not logged in
    
    @last-modified 2014-12-08
    @last-modified-by Neophytos Demetriou (neophytos@azet.sk)
} {

    upvar $where_clauseVar where_clause

    set criteria [list]

    im_absence_component__user_selection_helper \
        -user_selection $user_selection \
        -user_selection_idVar user_selection_id \
        -user_selection_typeVar user_selection_type \
        -hide_colors_pVar hide_colors_p \
        -current_user_id $current_user_id

    if {$hide_colors_p} {
        # show only approved and requested
        set absence_status_id [list [im_user_absence_status_active] [im_user_absence_status_requested]  [im_user_absence_status_planned]]
        lappend criteria "a.absence_status_id in ([template::util::tcl_to_sql_list $absence_status_id])"
    } else {
        if { $absence_status_id ne {} } {
            if { [llength $absence_status_id] == 1 } {
                lappend criteria "a.absence_status_id in ([template::util::tcl_to_sql_list [im_sub_categories $absence_status_id]])"
            } else {
                lappend criteria "a.absence_status_id in ([template::util::tcl_to_sql_list $absence_status_id])"
            }
        }
    }

    if { $absence_type_id ne {} && $absence_type_id > "0" } {
        lappend criteria "a.absence_type_id = :absence_type_id"
    }

    # temporary hack until I manage to refactor the code
    append where_clause [db_bind_var_substitution [im_where_from_criteria $criteria]]

}

ad_proc -private im_absence_component__user_selection_helper {
    -user_selection:required
    -user_selection_idVar:required
    -user_selection_typeVar:required
    {-hide_colors_pVar ""}
    {-user_nameVar ""}
    {-current_user_id ""}
} {
    Returns true if the current user can view the given selection. 
    Otherwise, it returns false. 
    
    @last-modified 2014-11.24
    @last-modified-by Neophytos Demetriou (neophytos@azet.sk)
} {

    upvar $user_selection_idVar user_selection_id
    upvar $user_selection_typeVar user_selection_type

    if {$user_nameVar ne {}} {
        upvar $user_nameVar user_name
    }

    if {$hide_colors_pVar ne {}} {
        upvar $hide_colors_pVar hide_colors_p
    }

    set user_selection_id ""
    set user_selection_type ""
    set user_name ""
    set hide_colors_p 0

    if {$current_user_id eq ""} {
        set current_user_id [auth::get_user_id]        
    }

    set can_add_all_p [im_permission $current_user_id "add_absences_all"]
    set can_view_all_p [expr { [im_permission $current_user_id "view_absences_all"] || $can_add_all_p }]

    # user_selection is required to be an integer
    # returns false, no one can view the component
    # with a non-integer selection
    if { [string is integer -strict $user_selection] } {

        # Figure out the object_type for the given object id, i.e. user_selection.
        set sql "select object_type from acs_objects where object_id = :user_selection"
        set object_type [db_string object_type $sql -default ""]

        switch $object_type {

            im_cost_center {

                set user_name [im_cost_center_name $user_selection]
                if {[im_manager_of_cost_center_p -user_id $current_user_id -cost_center_id $user_selection] || $can_view_all_p} {
                    # allow managers to view absences in their department
                    set user_selection_type "cost_center"
                    set user_selection_id $user_selection
                } else {
                    return false
                }

            }

            user {

                set user_name [im_name_from_user_id $user_selection]
                set user_selection_type user
                set user_selection_id $user_selection
                
                # Show only if user is an employee
                set owner_id $user_selection_id

                set read_p 0
                incr_if read_p {[im_permission $current_user_id "view_absences_all"]}
                incr_if read_p {[im_hr_or_supervisor_p -employee_id $owner_id]}
                incr_if read_p {$owner_id == $current_user_id}

                if { !$read_p } {
                    set hide_colors_p 1
                }
                
                # Check if they have a manager
				set sql "
                	select manager_id
                	from im_employees e
                	inner join im_cost_centers cc
                	on (cc.cost_center_id=e.department_id)
                	where employee_id = :owner_id
                "
                set exists_p [db_0or1row supervisor_and_cc_manager $sql]
                
                if { $exists_p } { 
	                incr_if read_p {$manager_id == $current_user_id}
	            }
    	            return $read_p

            }

            im_project {

                set project_manager_p [im_biz_object_member_p -role_id [im_biz_object_role_project_manager] $current_user_id $user_selection]
                if {$project_manager_p || $can_view_all_p} {
                    set user_name [db_string project_name "select project_name from im_projects where project_id = :user_selection" -default ""]
                    set hide_colors_p 1
                    set user_selection_type "project"
                    set user_selection_id $user_selection
                } else {
                    return false
                }

            }

			im_profile {
				set user_selection_type "profile"
				return $can_view_all_p
			}

            default {
                ad_return_complaint 1 "Invalid User Selection:<br>Value '$user_selection' is not a user_id, project_id, department_id or one of {mine|all|employees|providers|customers|direct reports}."
                return false
            }

        }
    } else {

        set add_absences_for_group_p [im_permission $current_user_id "add_absences_for_group"]
        set add_absences_all_p [im_permission $current_user_id "add_absences_all"]
        set view_absences_all_p [expr [im_permission $current_user_id "view_absences_all"] || $can_add_all_p]
        set add_absences_p [im_permission $current_user_id "add_absences"]

        switch $user_selection {
            mine { 
                set user_selection_id $current_user_id
                set user_selection_type user
                return true
            }
            all  -
            employees -
            providers -
            customers {
                set user_selection_type $user_selection
                return $can_view_all_p
            }
            {direct_reports} {

                set user_selection_type "direct_reports"

                set can_add_absences_direct_reports_p \
                    [im_permission $current_user_id "add_absences_direct_reports"]

                set can_view_absences_direct_reports_p \
                    [expr [im_permission $current_user_id "view_absences_direct_reports"] || $can_add_absences_direct_reports_p]

                return $can_view_absences_direct_reports_p

            }
        }

    }

    return true
}




ad_proc -private im_absence_component__user_selection {
    -where_clauseVar:required
    -user_selection:required
    -hide_colors_pVar:required
    {-user_selection_column "a.owner_id"}
    {-user_selection_typeVar ""}
    {-total_countVar ""}
    {-is_aggregate_pVar ""}
    {-im_where_from_criteria_keyword "and"}
    {-current_user_id ""}
} {
    @last-modified 2014-12-09
    @last-modified-by Neophytos Demetriou (neophytos@azet.sk)
} {

    upvar $where_clauseVar where_clause
    upvar $hide_colors_pVar hide_colors_p
    if { $user_selection_typeVar ne {} } {
        upvar $user_selection_typeVar user_selection_type
    }
    if { $total_countVar ne {} } {
        upvar $total_countVar total_count
    }
    if { $is_aggregate_pVar ne {} } {
        upvar $is_aggregate_pVar is_aggregate_p
    }

    if {$current_user_id eq ""} {
        set current_user_id [auth::get_user_id]        
    }

    im_absence_component__user_selection_helper \
        -user_selection $user_selection \
        -user_selection_idVar user_selection_id \
        -user_selection_typeVar user_selection_type \
        -hide_colors_pVar hide_colors_p \
        -current_user_id $current_user_id

    set criteria [list]

    set is_aggregate_p 0
    set total_count ""

    switch $user_selection_type {

        "all" {
            # Nothing.
            if { $total_countVar ne {} } {
                set total_count [db_string total_count "select count(1) from im_employees"]
            }
            set is_aggregate_p 1
        }

        "mine" {
            lappend criteria "${user_selection_column} = :current_user_id"
        }
        
        "profile" {
		   set sql "
				select	m.member_id
           		from	 	group_approved_member_map m
           		where	m.group_id = :user_selection
           	"
           
           	lappend criteria "(${user_selection_column} IN (${sql}) or a.group_id = :user_selection)"
           
           	if { $total_countVar ne {} } {
           		set total_count [db_string total_count "select count(1) from ($sql) t"]
           	}
           
           	set is_aggregate_p 1
           
        }
	        

        "employees" {

           set sql "
                select	m.member_id
                from	group_approved_member_map m
                where	m.group_id = [im_employee_group_id]
            "

            lappend criteria "(${user_selection_column} IN (${sql}) or a.group_id = [im_employee_group_id])"

            if { $total_countVar ne {} } {
                set total_count [db_string total_count "select count(1) from ($sql) t"]
            }

            set is_aggregate_p 1

        }

        "providers" {

            set sql "
                select	m.member_id 
                from	group_approved_member_map m 
                where	m.group_id = [im_freelance_group_id]
            "

            lappend criteria "${user_selection_column} IN (${sql})"

            if { $total_countVar ne {} } {
                set total_count [db_string total_count "select count(1) from ($sql) t"]
            }

            set is_aggregate_p 1

        }

        "customers" {

            set sql "
                select	m.member_id
                from	group_approved_member_map m
                where	m.group_id = [im_customer_group_id]
            "

            lappend criteria "${user_selection_column} IN (${sql})"

            if { $total_countVar ne {} } {
                set total_count [db_string total_count "select count(1) from ($sql) t"]
            }

            set is_aggregate_p 1

        }

        "direct_reports" {

            set sql "
                select employee_id from im_employees
                where (supervisor_id = :current_user_id OR employee_id = :current_user_id)
            "

            lappend criteria "${user_selection_column} in (${sql})"

            if { $total_countVar ne {} } {
                set total_count [db_string total_count "select count(1) from ($sql) t"]
            }


            set is_aggregate_p 1

        }  

        "cost_center" {

            set cost_center_id $user_selection_id
            set cost_center_list [im_cost_center_options -parent_id $cost_center_id]
            set cost_center_ids [list $cost_center_id]
            foreach cost_center $cost_center_list {
                lappend cost_center_ids [lindex $cost_center 1]
            }

            set sql "
                select employee_id 
                from im_employees 
                where department_id in ([template::util::tcl_to_sql_list $cost_center_ids]) 
                and employee_status_id = [im_employee_status_active] 
            "

            lappend criteria "${user_selection_column} in (${sql})"

            if { $total_countVar ne {} } {
                set total_count [db_string total_count "select count(1) from ($sql) t"]
            }

            set is_aggregate_p 1

        }

        "project" {

            set project_id $user_selection_id
            set project_ids [im_project_subproject_ids -project_id $project_id]
            set sql "
                select distinct object_id_two 
                from acs_rels 
                where object_id_one in ([template::util::tcl_to_sql_list $project_ids])
            "

            lappend criteria "${user_selection_column} in (${sql})"

            if { $total_countVar ne {} } {
                set total_count [db_string total_count "select count(1) from ($sql) t"]
            }

            set is_aggregate_p 1

        }

        "user" {
            set user_id $user_selection_id
            lappend criteria "${user_selection_column}=:user_id"
        }	    

        default  {
            # We shouldn't even be here, so just display his/her own ones
            lappend criteria "${user_selection_column} = :current_user_id"
        }

    }

    # temporary hack until I manage to refactor the code
    append where_clause [db_bind_var_substitution [im_where_from_criteria $criteria $im_where_from_criteria_keyword]]


}

ad_proc im_absence_component__timescale {
    {-where_clauseVar ""}
    {-start_dateVar ""}
    {-end_dateVar ""}
    {-where_clauseVar ""}
    {-num_daysVar ""}
    -timescale_date:required
    -timescale:required
} {
    @last-modified 2014-11-24
    @last-modified-by Neophytos Demetriou (neophytos@azet.sk)
} {

    foreach myVar {where_clause start_date end_date num_days} {
        set otherVar "${myVar}Var"
        if { [set $otherVar] ne {}} {
            upvar [set $otherVar] $myVar
        }
    }

    set criteria [list]

    set today_date [db_string today "select now()::date"]
    if {$timescale_date eq {}} {
	set timescale_date $today_date
    }

    set num_days ""
    set start_date $timescale_date
    set end_date $timescale_date
    switch $timescale {
        "all" {
	    # Limit the display to 365 days back and forward as you can always change by start date.
            set num_days 365
	    set start_date [db_string all "select to_date(:timescale_date,'YYYY-MM-DD') - :num_days::integer"]
	    set end_date [db_string all "select to_date(:timescale_date,'YYYY-MM-DD') + :num_days::integer"]
        }
        "today" { 
            set num_days 1
            set end_date $timescale_date
        }
        "next_3w" { 
            set num_days 21 
            set end_date [db_string 3w "select to_date(:timescale_date,'YYYY-MM-DD') + :num_days::integer"]
        }
        "last_3w" { 
            set num_days -21 
            set end_date $timescale_date
            set start_date [db_string 3w "select to_date(:timescale_date,'YYYY-MM-DD') + :num_days::integer"]
        }
        "past" { 
	    # Limit to the last 6 months, if you need to go further, change start date
            set num_days 185
	    set start_date [db_string past "select to_date(:timescale_date,'YYYY-MM-DD') - :num_days::integer"]
        }
        "future" { 
	    # We assume noone has planing ahead for more then one year, otherwise change start_date
            set num_days 365
	    set end_date [db_string future "select to_date(:timescale_date,'YYYY-MM-DD') + :num_days::integer"]
        }
        "last_3m" { 
            set num_days -93 
            set end_date $start_date
            set start_date [db_string 3w "select to_date(:timescale_date,'YYYY-MM-DD') + :num_days::integer"]
        }
        "next_3m" { 
            set num_days 93 
            set end_date [db_string 3w "select to_date(:timescale_date,'YYYY-MM-DD') + :num_days::integer"]
        }
        default {
            set num_days 21
        }
    }

    # Limit to start-date and end-date
    if {$start_date ne {}} { lappend criteria "a.end_date::date >= :start_date" }
    if {$end_date ne {}} { lappend criteria "a.start_date::date <= :end_date" }

    # Hard Limit for the start_date 
    set max_days [parameter::get -parameter HideAbsencesOlderThanDays -default "365"]
    set max_days_interval "$max_days days"
    lappend criteria "a.start_date::date > now() - :max_days_interval::interval"

    # temporary hack until I manage to refactor the code
    append where_clause [db_bind_var_substitution [im_where_from_criteria $criteria]]

}


ad_proc im_absence_component__order_by_clause {order_by} {
    @last-update 2014-11-24
    @modifying-user Neophytos Demetriou (neophytos@azet.sk)
} {
    set order_by_clause ""
    switch $order_by {
        "Name" { set order_by_clause "order by upper(absence_name), owner_name" }
        "User" { set order_by_clause "order by owner_name, start_date" }
        "Date" { set order_by_clause "order by start_date, owner_name" }
        "Start" { set order_by_clause "order by start_date" }
        "End" { set order_by_clause "order by end_date" }
        "Type" { set order_by_clause "order by absence_type, owner_name" }
        "Status" { set order_by_clause "order by absence_status, owner_name" }
    }
    return $order_by_clause
}


ad_proc -private im_supervisor_of_employee_p {
    -supervisor_id
    -employee_id
} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {
    set sql "select true from im_employees where employee_id=:employee_id and supervisor_id=:supervisor_id"
    return [db_string supervisor_p $sql -default false]
}

ad_proc -private im_manager_of_employee_p {
    -manager_id
    -employee_id
} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {
    set sql "
        select true 
        from im_employees e 
        inner join im_cost_centers cc 
        on (cc.cost_center_id=e.department_id) 
        where employee_id=:employee_id and manager_id=:supervisor_id
    "
    return [db_string supervisor_p $sql -default false]
}

ad_proc -public im_absence_cube_component {
    -user_selection:required
    {-absence_status_id "" }
    {-absence_type_id "" }
    {-timescale "next_3w" }
    {-timescale_date "" }
    {-hide_colors_p 0}
} {

    Makes use of im_absence_cube to return a rendered cube with 
    a graphical absence display for users.

    Copied and modified im_absence_vacation_balance_component to
	ensure that it parses the include in a similar manner.

} {

    if {$timescale_date eq {}} {
        set timescale_date [db_string today "select now()::date"]
	}

    if { ![im_absence_component_view_p -user_selection $user_selection] } {
#        return "You do not have enough privileges to view this component"
	return ""
    }

    set params [list \
		    [list user_selection $user_selection] \
			[list absence_status_id $absence_status_id] \
			[list absence_type_id $absence_type_id] \
			[list timescale $timescale] \
			[list timescale_date $timescale_date] \
			[list user_selection $user_selection] \
			[list hide_colors_p $hide_colors_p] \
		    [list return_url [im_url_with_query]] \
    ]

    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/absence-cube"]
    return [string trim $result]
	}

ad_proc -public im_absence_list_component {
    -user_selection:required
    {-absence_status_id "" }
    {-absence_type_id "" }
    {-timescale "" }
    {-timescale_date "" }
    {-hide_colors_p 0}
    {-order_by ""}
} {

    @last-modified 2014-11-24
    @last-modified-by Neophytos Demetriou (neophytos@azet.sk)

} {

    if { ![im_absence_component_view_p -user_selection $user_selection] } {
#        return "You do not have enough privileges to view this component"
	return ""
    }

    set params [list \
			[list user_selection $user_selection] \
			[list absence_status_id $absence_status_id] \
			[list absence_type_id $absence_type_id] \
			[list timescale $timescale] \
			[list timescale_date $timescale_date] \
			[list hide_colors_p $hide_colors_p] \
			[list order_by $order_by] \
		    [list return_url [im_url_with_query]] \
    ]

    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/absences-list"]
    return [string trim $result]
	}

ad_proc im_absence_fg_color {
   col
} {
    moved from im_absence_cube_legend
} {

    if { [string length $col] == 6} {
        # Transform RGB Hex-Values (e.g. #a3b2c4) into Dec-Values
        set r_bg [expr 0x[string range $col 0 1]]
        set g_bg [expr 0x[string range $col 2 3]]
        set b_bg [expr 0x[string range $col 4 5]]
    } elseif { [string length $col] == 3 } {
        # Transform RGB Hex-Values (e.g. #a3b) into Dec-Values
        set r_bg [expr 0x[string range $col 0 0]]
        set g_bg [expr 0x[string range $col 1 1]]
        set b_bg [expr 0x[string range $col 2 2]]
    } else {
        # color codes can't be parsed -> set a middle value
        set r_bg 127
        set g_bg 127
        set b_bg 127
    }
    # calculate a brightness-value for the color
    # if brightness > 127 the foreground color is black, if < 127 the foreground color is white
    set brightness [expr $r_bg * 0.2126 + $g_bg * 0.7152 + $b_bg * 0.0722]
    set col_fg "fff"
    if {$brightness >= 127} {set col_fg "000"}

    return $col_fg
}

ad_proc im_absence_render_cell {
    bg_color
    {fg_color "#fff"}
    {str "&nbsp;"}
    {align "center"}
    {extra_style "padding:3px;"}
} {
    Renders a single report cell, depending on value.
    Takes the color from absences color lookup.
} {
    if { $bg_color ne {} } {
        return "<td style='text-align:${align}; background-color:\#$bg_color; color:\#$fg_color;${extra_style}'>$str</td>"
    } else {
        return "<td>&nbsp;</td>\n"
    }
}

ad_proc -public im_absence_day_list {
    {-date_format:required}
    {-num_days:required}
    {-start_date:required}
} {
    get a day_list
} {
    return [util_memoize \
        [list im_absence_day_list_helper \
            -date_format $date_format \
            -num_days $num_days \
            -start_date $start_date]]
}

ad_proc -public im_absence_day_list_helper {
    {-date_format:required}
    {-num_days:required}
    {-start_date:required}
} {
    get a day_list
} {

    set day_list [list]
    for {set i 0} {$i < $num_days} {incr i} {
        db_1row date_info "
        	    select 
        		to_char(:start_date::date + :i::integer, :date_format) as date_date,
        		to_char(:start_date::date + :i::integer, 'Day') as date_day,
        		to_char(:start_date::date + :i::integer, 'dd') as date_day_of_month,
        		to_char(:start_date::date + :i::integer, 'Mon') as date_month,
        		to_char(:start_date::date + :i::integer, 'YYYY') as date_year,
        		to_char(:start_date::date + :i::integer, 'Dy') as date_weekday
    "

        set date_month [lang::message::lookup "" intranet-timesheet2.$date_month $date_month]
        lappend day_list [list $date_date $date_day_of_month $date_month $date_year]
    }
    return $day_list
}

ad_proc -public im_absence_calendar_component {
    {-owner_id ""}
    {-year ""}
    {-absence_status_id "" }
    {-absence_type_id "" }
    {-hide_explanation_p "0"}
} {

   Displays a yearly calendar of absences for a user. 
   Uses the same color coding as the absence cube, but
   instead of displaying multiple users, it works only 
   for one user.

} {

    if { $year eq {} } {
        set year [clock format [clock seconds] -format "%Y"]
    }

    set user_selection $owner_id
    set current_user_id [auth::get_user_id]
    if { ![im_absence_component_view_p -user_selection $user_selection] } {
#        return "You do not have enough privileges to view this component"
	return ""
	}

    set params \
        [list \
		    [list user_selection $user_selection] \
			[list year $year] \
			[list absence_status_id $absence_status_id] \
			[list absence_type_id $absence_type_id] \
            [list hide_explanation_p $hide_explanation_p]]

    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/absence-calendar"]
    return [string trim $result]
    }


ad_proc -public im_absence_vacation_balance_component {
    -user_id_from_search:required
} {
    Returns a HTML component showing the number of days left
    for the user
} {

    # Show only if user is an employee
    if { ![im_user_is_employee_p $user_id_from_search] } { return "" }

    set current_user_id [auth::get_user_id]
    # This is a sensitive field, so only allows this for the user himself
    # and for users with HR permissions.

    set read_p 0
    if {$user_id_from_search == $current_user_id} { set read_p 1 }
    if {[im_permission $current_user_id view_hr]} { set read_p 1 }
    if {!$read_p} { return "" }

    set params [list \
		    [list user_id_from_search $user_id_from_search] \
		    [list return_url [im_url_with_query]] \
    ]

    set result [ad_parse_template -params $params "/packages/intranet-timesheet2/www/absences/vacation-balance-component"]
    return [string trim $result]
}



ad_proc -public im_absence_formatted_duration_to_days {
    days_formatted
} {
    Converts a time string to days.
    Examples: '1 day', '4 hours', '1' (=one day), '0.5' (=half a day)
} {
    set days_formatted_1 [string trim [string tolower $days_formatted]]
    set days_formatted_2 [string map {days d day d hours h hour h} $days_formatted_1]

    set days ""
    if {[regexp {^(.+)d} $days_formatted_2 match day_string]} { 
	catch {
	    set days [expr 1.0 * [string trim $day_string]]
	}
    }
    if {[regexp {^(.+)h} $days_formatted_2 match hour_string]} { 
	catch {
	    set days [expr [string trim $hour_string] / 8.0]
	}
    }
    if {"" eq $days} { 
	catch {
	    set days [expr 1.0 * $days_formatted_2]
	}
    }

    return $days
}



ad_proc -public im_absences_assign_vacation_replacement {
    -task_id
} {
    Assigns a vacation replacement to a task
} {
    set current_user_id [ad_conn user_id]

    # ---------------------------------------------------------------------
    # Does the assigned user have an absence currently?
    #
    set assignee_html ""
    set assignee_sql "
	select	ua.vacation_replacement_id
	from	wf_tasks t,
		wf_task_assignments ta,
		im_user_absences ua
	where	t.task_id = :task_id and
		ta.task_id = t.task_id and
		ta.party_id = ua.owner_id and
		now()::date between ua.start_date and ua.end_date
    "
    db_foreach assignee $assignee_sql {
        if {$current_user_id eq $vacation_replacement_id} {
	    db_string wf_assig "select workflow_case__add_task_assignment (:task_id, :vacation_replacement_id, 'f')"
	}
    }
}


# packages/intranet-timesheet2/tcl/intranet-leave-entitlement-procs.tcl

## Copyright (c) 2011, cognovis GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
# 

ad_library {
    
    Procedures for leave entitlements
    
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2013-11-20
    @cvs-id $Id$
}

ad_proc -public im_leave_entitlement_user_component {
    -user_id:required
} {
    Returns a HTML component showing the leave entitlements
    for the user
} {
    set current_user_id [auth::get_user_id]
    # This is a sensitive field, so only allows this for the user himself
    # and for users with HR permissions.

    set read_p 0
    if {$user_id == $current_user_id} { set read_p 1 }
    if {[im_permission $current_user_id view_hr]} { set read_p 1 }

    # Only show for employees
    if {![im_user_is_employee_p $user_id]} { set read_p 0 }

    if {!$read_p} { return "" }

    set params [list \
		    [list user_id_from_search $user_id] \
		    [list return_url [im_url_with_query]] \
    ]

    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/leave-entitlements"]
    return [string trim $result]
}


ad_proc -public im_leave_entitlement_absence_balance_component {
    -user_id:required
} {
    Returns a HTML component showing the balance of his entitlements and how much of it is spend
} {

    # Show only if user is an employee
    if { ![im_user_is_employee_p $user_id] } { return "" }

    set current_user_id [auth::get_user_id]
    # This is a sensitive field, so only allows this for the user himself
    # and for users with HR permissions.

    set read_p 0
    if {$user_id == $current_user_id} { set read_p 1 }
    if {[im_permission $current_user_id view_hr]} { set read_p 1 }
    if {!$read_p} { return "" }

    set params [list \
		    [list user_id $user_id] \
		    [list return_url [im_url_with_query]] \
    ]

    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/absence-balance-component"]

    return [string trim $result]
}


ad_proc -public im_leave_entitlement_remaining_days {
    -user_id:required
    -absence_type_id:required
    {-approved_p "0"}
    {-ignore_absence_ids ""}
    {-booking_date ""}
    {-include_planned_p "0"}
} {
    Returns the number of remaining days for the user of a certain absence type
    
    @param approved_p Only calculate based on the approved vacation days
    @param booking_date Define which leave entitlements should be included. Defaults to current date (everything earned up until today)
} {
    return [util_memoize [list im_leave_entitlement_remaining_days_helper -absence_type_id $absence_type_id -user_id $user_id -approved_p $approved_p -ignore_absence_ids $ignore_absence_ids -booking_date $booking_date -include_planned_p $include_planned_p] 5]
}


ad_proc -public im_leave_entitlement_remaining_days_helper {
    -user_id:required
    -absence_type_id:required
    {-approved_p "0"}
    {-ignore_absence_ids ""}
    {-booking_date ""}
    {-requested_daysVar ""}
    {-include_planned_p "0"}
} {
    Returns the number of remaining days for the user of a certain absence type for the year in which the absence is requested.
    
    @param approved_p Only calculate based on the approved vacation days
    @param booking_date Define which leave entitlements should be included. Defaults to current date (everything earned up until today)
} {

    if { $requested_daysVar ne {} } {
        upvar $requested_daysVar requested_days
    }

    set current_year [dt_systime -format "%Y"]
    set eoy "${current_year}-12-31"
    set soy "${current_year}-01-01"
    
    # By default calculate all entitlements from the past
    set booking_date_sql "and booking_date <= to_date(:eoy,'YYYY-MM-DD')"
    
    # Calculate against all absences in the past
    set date_sql "and start_date::date <=:eoy"
    
    if {$booking_date ne ""} {
        set booking_year [string range $booking_date 0 3]
        if {$booking_year > $current_year} {
            set eoy "${booking_year}-12-31"
            set soy "${booking_year}-01-01"

            # This is a booking for a future year
            # Only calculate entitlements for that year
            set booking_date_sql "and booking_date <= to_date(:eoy,'YYYY-MM-DD') and to_date(:soy,'YYYY-MM-DD') <= booking_date"
            set date_sql "and start_date::date <=:eoy and end_date::date >=:soy"
        }
    }
    
    
    if { $absence_type_id == [im_user_absence_type_overtime] } {
        
        # for the overtime category (and child categories) we are not 
        # filtering the leave entitlements only for the current / booking
        # year, but use all of them
        
        set booking_date_sql ""
        set date_sql ""
        
    } 
    
    set sql "
            select coalesce(sum(l.entitlement_days),0) as absence_days 
            from im_user_leave_entitlements l 
            where leave_entitlement_type_id = :absence_type_id 
            and owner_id = :user_id 
            $booking_date_sql
        "

    set entitlement_days [db_string entitlement_days $sql -default 0]    

	set absence_type [im_category_from_id $absence_type_id]
    
    # Ignore the balance for bank holidays

    set vacation_category_ids [im_sub_categories 5000]
    set exclude_category_ids [db_list categories "
    	select
                    category_id
    	from
    		im_categories c
    	where
                    category_type = 'Intranet Absence Type' and category_id not in ([template::util::tcl_to_sql_list $vacation_category_ids])
    "]

	# Check if we have a workflow and then only use the approved days
	set wf_key [db_string wf "select trim(aux_string1) from im_categories where category_id = :absence_type_id" -default ""]
	set wf_exists_p [db_string wf_exists "select count(*) from wf_workflows where workflow_key = :wf_key"]
    
    # We need to ignore this absence_id from the calculation of
    # absence days. Usually during an edit
    if {$ignore_absence_ids eq ""} {
	    set ignore_absence_sql ""
    } else {
        set ignore_absence_sql "and absence_id not in ([template::util::tcl_to_sql_list $ignore_absence_ids])"
    }
    
    set requested_days ""
    
    # Include planned days when checking for absences
    set active_status_ids [im_sub_categories [im_user_absence_status_active]]
    set requested_status_ids [im_sub_categories [im_user_absence_status_requested]]
    if {$include_planned_p} {
        lappend active_status_ids [im_user_absence_status_planned]
    }
    
    if {$wf_exists_p} {

        set absence_days [db_string absence_days "select coalesce(sum(duration_days),0)
            from im_user_absences 
            where absence_type_id = :absence_type_id
            and absence_status_id in ([template::util::tcl_to_sql_list $active_status_ids] )
            and owner_id = :user_id
            $date_sql
            $ignore_absence_sql" -default 0]

        set requested_days [db_string requested_days "select coalesce(sum(duration_days),0) 
            from im_user_absences 
            where absence_type_id = :absence_type_id
            and absence_status_id in ([template::util::tcl_to_sql_list $requested_status_ids])
            and owner_id = :user_id
            $date_sql
            $ignore_absence_sql" -default 0]

        set remaining_days [expr $entitlement_days - $absence_days]
        if {!$approved_p} {
            # We need to substract the requested days as well
            set remaining_days [expr $remaining_days - $requested_days]
        }
    } else {
        set absence_days [db_string absence_days "select coalesce(sum(duration_days),0)
            from im_user_absences 
            where absence_type_id = :absence_type_id
            and absence_status_id in ([template::util::tcl_to_sql_list $active_status_ids])
            and owner_id = :user_id
            $date_sql
            $ignore_absence_sql" -default 0]
        set remaining_days [expr $entitlement_days - $absence_days]
    } 

    ds_comment "$absence_days :: $entitlement_days :: $absence_type_id"

    return $remaining_days
}

ad_proc -public im_leave_entitlement_create_yearly_vacation {
    -year
} {
    Small Procedure to create the yearly vacation for each user if not already created
} {
    set booking_date "${year}-01-01"
    set leave_entitlement_name "Annual Leave"
    set leave_entitlement_status_id "[im_user_absence_status_active]"
    set leave_entitlement_type_id "5000"
    set description "Automatically generated"
    set user_id [ad_conn user_id]
    
    set employee_vacation_list [db_list_of_lists employee_vacation "select employee_id, vacation_days_per_year from im_employees 
        where employee_status_id = [im_employee_status_active]
        and vacation_days_per_year is not null
        and employee_id not in (select owner_id from im_user_leave_entitlements 
            where booking_date = :booking_date and leave_entitlement_name = :leave_entitlement_name)"]
    
    foreach employee_vacation $employee_vacation_list {
        set leave_entitlement_id [db_nextval acs_object_id_seq]
        
        set owner_id [lindex $employee_vacation 0]
        set entitlement_days [lindex $employee_vacation 1]
        
        db_transaction {
	        set absence_id [db_string new_absence "
	    	    SELECT im_user_leave_entitlement__new(
                :leave_entitlement_id,
                'im_user_leave_entitlement',
                now(),
                :user_id,
                '[ns_conn peeraddr]',
                null,
                :leave_entitlement_name,
                :owner_id,
                :booking_date,
                :entitlement_days,
                :leave_entitlement_status_id,
                :leave_entitlement_type_id,
                :description
                )"]

            db_dml update_object "
	            update acs_objects set
			    last_modified = now()
                where object_id = :absence_id"
	
            # Audit the action
            im_audit -object_type im_user_leave_entitlement -action after_create -object_id $leave_entitlement_id -status_id $leave_entitlement_status_id -type_id $leave_entitlement_type_id
        }
    }
}

namespace eval cog::absence {

    ad_proc -public new {
        -current_user_id:required
        -absence_name:required
        { -owner_id ""}
        { -group_id ""}
        { -absence_type_id 5000 }
        { -absence_status_id 16009}
        -start_date:required
        -end_date:required
        { -description ""}
        { -contact_info ""}
        { -vacation_replacement_id ""}
        { -manager_vacation_replacement_id ""}
    } {
        Creates a new absence for the owner or the group

        @param current_user_id User who is currently logged in. Results in an error if the current user is not allowed to add a vacation for owner or group
        @param absence_name Name of the absence, used to identify it
        @param owner_id Who is the absence for.
        @param group_id Group for whom we want to add the absence. Alternative to owner. Will not be used if owner is provided.
        @param absence_type_id Type of absence, defaults to vacation
        @param absence_status_id Status of the absence, defaults to planned 
        @param start_date First day of the absence
        @param end_date Last day of the absence
        @param description Additional description for the absence
        @param vacation_replacement_id Which user is replacing the current user in case of absence
    } {
        set add_absences_for_group_p [im_permission $current_user_id "add_absences_for_group"]
        set write [im_permission $current_user_id "add_absences"]
        set admin [im_is_user_site_wide_or_intranet_admin $current_user_id]
        
        if {!$write} {
            cog_log Warning "$current_user_id tried to add an absence which is not allowed"
            return ""
        }

        if {!$add_absences_for_group_p} {
            set group_id ""
        }

        if {$group_id eq ""} {
            if {$owner_id eq ""} {
                set owner_id $current_user_id
            }
        } else {
            set owner_id ""
        }

        set absence_id [db_nextval acs_object_id_seq]
        callback im_user_absence_before_create -object_id $absence_id -status_id $absence_status_id -type_id $absence_type_id

        set absence_id [db_string new_absence "
            SELECT im_user_absence__new(
                :absence_id,
                'im_user_absence',
                now(),
                :current_user_id,
                '[ns_conn peeraddr]',
                null,
                :absence_name,
                :owner_id,
                :start_date,
                :end_date,
                :absence_status_id,
                :absence_type_id,
                :description,
                :contact_info
            )
        "]

        set duration_days [im_absence_calculate_absence_days -start_date $start_date -end_date $end_date -absence_id $absence_id]
        db_dml update_absence "
                UPDATE im_user_absences SET
                        absence_name = :absence_name,
                        owner_id = :owner_id,
                        start_date = :start_date,
                        end_date = :end_date,
                        duration_days = :duration_days,
                        group_id = :group_id,
                        absence_status_id = :absence_status_id,
                        absence_type_id = :absence_type_id,
                        description = :description,
                        contact_info = :contact_info,
                        manager_vacation_replacement_id = :manager_vacation_replacement_id,
                        vacation_replacement_id = :vacation_replacement_id
                WHERE
                        absence_id = :absence_id
        "

        db_dml update_object "
            update acs_objects set
                last_modified = now(),
                modifying_user = :current_user_id,
                modifying_ip = '[ad_conn peeraddr]'
            where object_id = :absence_id
        "

        set wf_key [db_string wf "select trim(aux_string1) from im_categories where category_id = :absence_type_id" -default ""]
        set wf_exists_p [db_string wf_exists "select count(*) from wf_workflows where workflow_key = :wf_key"]
        # Skip the workflow creation if it is a planned absence
        # Probably other customers want to have a workflow for planned absences, though that
        # Defies the purpose of undisturbed planning by the user
        if {$wf_exists_p && $absence_status_id != [im_user_absence_status_planned]} {
            set context_key ""
            set case_id [wf_case_new \
                            $wf_key \
                            $context_key \
                            $absence_id
                        ]

            # Determine the first task in the case to be executed and start+finisch the task.
            im_workflow_skip_first_transition -case_id $case_id
        }

        callback absence_on_change \
            -absence_id $absence_id \
            -absence_type_id $absence_type_id \
            -user_id $owner_id \
            -start_date $start_date \
            -end_date $end_date \
            -duration_days $duration_days \
            -transaction_type "add"

        # Audit the action
        im_audit -object_type im_user_absence -action after_create -object_id $absence_id -status_id $absence_status_id -type_id $absence_type_id
        return $absence_id
    }

    ad_proc -public delete {
        -absence_id:required
        -current_user_id:required
    } {
        set valid_p [db_0or1row hours "select owner_id, group_id, absence_status_id from im_user_absences where absence_id = :absence_id"]
        if {!$valid_p} {
            return 0
        }
        
        set perm_table [im_absence_new_page_wf_perm_table]
        set perm_set [cog::workflow::object_permissions  -object_id $absence_id  -perm_table $perm_table -user_id $current_user_id]
        if {[lsearch $perm_set "d"] <0} {
            return 0
        }

        set absence_under_wf_control_p [db_string wf_control "
            select      count(*)
            from    wf_cases
            where   object_id = :absence_id"]

        # Check if we have a planned absence
        if {[parameter::get_from_package_key -package_key intranet-timesheet2 -parameter "CancelAbsenceP" -default 1] && $absence_status_id ne [im_user_absence_status_planned]} {

            # We just cancel the workflow and not delete it
            callback im_user_absence_before_nuke  -object_id $absence_id -status_id [im_user_absence_status_deleted]

            # Set the workflow to finished
            if {$absence_under_wf_control_p} {
                set case_id [db_string case "select min(case_id) from wf_cases where object_id = :absence_id"]

                if {[catch {wf_case_cancel -msg "Absence was cancelled by [im_name_from_user_id $current_user_id]" $case_id}]} {
                    #Record the change manually, as the workflow did fail (probably because the case is already closed
                    im_workflow_new_journal -case_id $case_id -action "cancel absence" -action_pretty "Cancel Absence" -message "Absence was cancelled by [im_name_from_user_id $current_user_id]"
                    return 0
                }
            }

            # Update the vacation status to cancelled
            db_dml cancel_absence "update im_user_absences set absence_status_id = [im_user_absence_status_deleted] where absence_id = :absence_id"
            im_audit -object_type im_user_absence -action after_nuke -object_id $absence_id -status_id [im_user_absence_status_deleted]

        } else {
            db_transaction {
       
                callback absence_on_change \
                    -absence_id $absence_id \
                    -absence_type_id "" \
                    -user_id "" \
                    -start_date "" \
                    -end_date "" \
                    -duration_days "" \
                    -transaction_type "remove"

                db_dml del_tokens "delete from wf_tokens where case_id in (select case_id from wf_cases where object_id = :absence_id)"
                db_dml del_case "delete from wf_cases where object_id = :absence_id"
                db_string absence_delete "select im_user_absence__delete(:absence_id)"
                im_audit -object_type im_user_absence -action after_nuke -object_id $absence_id

            } on_error {
                ad_return_error "Error deleting absence" "<br>Error:<br>$errmsg<br><br>"
                return 0
            }
        }

        return 1
    }
}