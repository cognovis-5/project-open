
ad_library {
    Procedures for supporting with invoices
    @author malte.sussdorff@cognovis.de
}

ad_proc -public im_cost_default_currency {} { Returns system currency } {
    return [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency"]
}

ad_proc cog_project_finance_component { 
    {-show_details_p 1}
    {-show_summary_p 1}
    {-show_admin_links_p 1}
    {-no_timesheet_p 0}
    {-view_name ""}
    {-disable_view_standard_p 0}
    {-disable_view_finance_p 0}
    {-user_id ""}
    {-return_url ""}
    -project_id 
} {
    Returns a HTML table containing a detailed summary of all
    financial activities of the project. <p>

    ToDo:
    - Add DynView dynamic columns and remove parameters

    The complexity of this component comes from several sources:
    <ul>
    <li>We need to sum up the financial items and sort them into 
        several
        "buckets" that correspond to the different cost types
        such as "customer invoices", "provider purchase orders",
        internal "timesheet costs" etc.
    <li>We can have costs and financial documents (see doc.) in
        several currencies, so we can't just add these together.
        Instead, we need to maintain separate sums per cost type
        and currency.
        Also, costs may have NULL cost values (timesheet costs
        from persons whithout the "hourly cost" defined).
    </ul>

} {
    if {$user_id eq ""} {
	set user_id [ad_conn user_id]
    }


    if {$return_url eq ""} {
        set return_url [im_url_with_query]
    }
    set params [list [list user_id $user_id] [list project_id $project_id] [list show_details_p $show_details_p] [list show_summary_p $show_summary_p] [list show_admin_links_p $show_admin_links_p] [list no_timesheet_p $no_timesheet_p] [list view_name $view_name] [list disable_view_standard_p $disable_view_standard_p] [list disable_view_finance_p $disable_view_finance_p] [list return_url $return_url]]
    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/project-finance"]
    return [string trim $result]
}


ad_proc -public im_cost_status_accepted {} {
    Returns the accepted cost status
} {
    return 3820
}

ad_proc -public im_cost_status_cancelled {} {
    Returns the cancelled cost status
} {
    return 3819
}   

ad_proc -public im_cost_status_send {} {
    Returns the send cost status
} {
    return 3815
}

namespace eval cog::invoice {

    ad_proc -public update_rounded_amount {
        -invoice_id:required
    } {
        Updates the invoice along with the project caches

        @param invoice_id Invoice which we have changed / created and need to get amounts for
    } {
        # Get the rounding factor for the invoice
        set currency [db_string currency "select currency from im_costs where cost_id = :invoice_id" -default "[parameter::get_from_package_key -parameter "DefaultCurrency" -package_key "intranet-cost"]"]
        set rf [im_invoice_rounding_factor -currency $currency]

        # Calculate the subtotal
        set subtotal [db_string subtotal "
            select  sum(round(price_per_unit * item_units * :rf) / :rf)
            from    im_invoice_items
            where   invoice_id = :invoice_id
        "]

        # Update the total amount, including surcharge and discount
        set update_invoice_amount_sql "
            update im_costs set
                    amount = :subtotal
            where cost_id = :invoice_id
        "

        db_dml update_invoice_amount $update_invoice_amount_sql
        
        # Update the vat amount
        set vat [db_string vat "select aux_int1 as vat from im_categories c, im_costs co where co.vat_type_id = c.category_id and co.cost_id = :invoice_id" -default ""]
        if {$vat eq ""} {
            # try the original vat from the cost
            set vat [db_string vat_from_cost "select vat from im_costs where cost_id = :invoice_id" -default 0]
        }

        set update_invoice_vat_sql "
            update im_costs set
                vat = :vat,
                vat_amount = amount + round(amount * :vat::numeric) / 100.0
            where cost_id = :invoice_id"
        db_dml update_invoice_vat $update_invoice_vat_sql

        set project_id [db_string project "select project_id from im_costs where cost_id = :invoice_id" -default ""]
        if {$project_id ne ""} {
            im_cost_update_project_cost_cache $project_id
        }
    }

    ad_proc -public send_email {
        -invoice_id
        {-recipient_id ""}
        {-from_addr ""}
        {-cc_addr ""}
        {-user_id ""}
    } {
        Send out an E-Mail to the recipient for with the invoice attached
        
        @param invoice_id cost_id of the invoice to be sent
        @param recipient_id Recipient of the invoice mail. Defaults to company_contact_id of the invoice
        @param from_addr Sender of the invoice mail. Defaults to current user mail
        @param cc_addr Optional cc email address. Useful for receiving copies.
        @param user_id Current user_id
    } {
        set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id]

        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }

        if {"" == $recipient_id} {
            set recipient_id [db_string company_contact_id "select company_contact_id from im_invoices where invoice_id = :invoice_id" -default $user_id]
        }

        db_1row get_recipient_info "select first_names, last_name, email as to_addr from cc_users where user_id = :recipient_id"

        db_1row invoice_info "select invoice_nr,last_modified from im_invoices,acs_objects where invoice_id = :invoice_id and invoice_id = object_id"

        if {![db_0or1row related_projects_sql "
                select distinct
                r.object_id_one as project_id,
                p.project_name,
                project_lead_id,
                im_name_from_id(project_lead_id) as project_manager,
                p.project_nr,
                p.parent_id,
                p.description,
                trim(both p.company_project_nr) as customer_project_nr
            from
                acs_rels r,
                im_projects p
            where
                r.object_id_one = p.project_id
                and r.object_id_two = :invoice_id
                order by project_id desc
                limit 1
        "]} {
            set project_name ""
            set project_manager [im_name_from_user_id $user_id]
            set project_lead_id $user_id
            set customer_project_nr ""
            set project_nr ""
        }

        if {"" == $from_addr} {
            set from_addr [party::email -party_id $user_id]
        }
        
        # Get the type information so we can get the strings
        set invoice_type_id [db_string type "select cost_type_id from im_costs where cost_id = :invoice_id"]
        
        set recipient_locale [lang::user::locale -user_id $recipient_id]
    
        set salutation_pretty [im_invoice_salutation -person_id $recipient_id]
        set subject [lang::util::localize "#intranet-invoices.invoice_email_subject_${invoice_type_id}#" $recipient_locale]
        set body [lang::util::localize "#intranet-invoices.invoice_email_body_${invoice_type_id}#" $recipient_locale]
        set message_id [intranet_chilkat::send_mail \
            -to_addr $to_addr -from_addr $from_addr -cc_addr $cc_addr \
            -subject $subject -body $body -file_ids $invoice_revision_id \
            -object_id $invoice_id]

        if {$message_id ne ""} {
            cog::callback::invoke -object_type "im_invoice" -object_id $invoice_id -action after_update -type_id $invoice_type_id
        }
        return $message_id
    }

    ad_proc -public create {
        -provider_contact_id:required
        -customer_contact_id:required
        { -provider_id "" }
        { -customer_id "" }
        {-invoice_type_id "3700"}
        { -invoice_status_id "" }
        { -project_id "" }
        { -cost_center_id "" }
        { -currency "" }
        { -note "" }
        { -delivery_date "" }
        { -effective_date "" }
        { -current_user_id ""}
        { -payment_term_id "" }
        { -payment_method_id "" }
        { -template_id ""}
    } {
        Create a new invoice

        @param provider_contact_id Who is the sender / provider of the invoice
        @param provider_id Which company is sending the invoice
        @param company_contact_id Who is the recipient of the invoice (actual user)
        @param customer_id For which company are we creating the invoice
        @param invoice_type_id Type of invoice (defaults to invoice)
        @param invoice_status_id Status of the invoice (defaults to created)
        @param cost_center_id Cost Center, defaults to project_cost_center_id
        @param project_id ProjectId with which to link the invoice document
        @param note additional note for the financial document
        @param effective_date date The actual invoice date (to be printed on the invoice pdf)
        @param delivery_date when was the work delivered for the invoice
        @param template_id category "Intranet Cost Template" Template used for printing the invoice
        @param payment_method_id category "Intranet Payment Type" Method of payment
        @param payment_term_id category "Intranet Payment Term" Terms for the payment (aka how fast has the payment to be made)

    } {

        if {$provider_contact_id eq "" || $customer_contact_id eq ""} {
            cog_log Error "You need to provide both provider_contact_id ($provider_contact_id) and customer_contact_id ($customer_contact_id)"
            return ""
        }

        if {$current_user_id eq ""} {
            set current_user_id [auth::get_user_id]
        }
        
        if {$invoice_status_id eq ""} {
            set invoice_status_id [im_cost_status_created]
        }

        if {$cost_center_id eq ""} {
            if {$project_id ne ""} {
                set cost_center_id [db_string cost_center_id "select project_cost_center_id from im_projects where project_id = :project_id" -default ""]
            }
        }

        if {$provider_id eq ""} {
            set provider_id [db_string select_company {
                select company_id from im_companies 
                where primary_contact_id = :provider_contact_id
                or accounting_contact_id = :provider_contact_id
                limit 1
            } -default 0]
        }

        if {$provider_id eq 0} {
            set provider_id [db_string select_company {
                select  c.company_id
                from	    acs_rels r,
                        im_companies c
                where	r.object_id_one = c.company_id
                and     r.object_id_two = :provider_contact_id
                and     r.rel_type = 'im_company_employee_rel'
                limit 1
            } -default 0]
        }

        if {$customer_id eq ""} {
            set customer_id [db_string select_company {
                select company_id from im_companies 
                where primary_contact_id = :customer_contact_id
                or accounting_contact_id = :customer_contact_id
                limit 1
            } -default 0]
        }
        
        if {$customer_id eq 0} {
            set customer_id [db_string select_company {
                select  c.company_id
                from	    acs_rels r,
                        im_companies c
                where	r.object_id_one = c.company_id
                and     r.object_id_two = :customer_contact_id
                and     r.rel_type = 'im_company_employee_rel'
                limit 1
            } -default 0]
        }

        if {$provider_id eq 0 || $customer_id eq 0} {
            cog_log Error "Could not find a provider or customer for $provider_contact_id and $customer_contact_id"
            return ""
        }

        set provider_type_ids [im_sub_categories 3710]
        
        if {[lsearch $provider_type_ids $invoice_type_id]<0} {
            # This is going to a customer, use the locale of the contact_recipient
            set user_id $provider_contact_id
            set company_contact_id $customer_contact_id
            set company_id $customer_id
        } else {
            if {$currency eq ""} {
                if {[im_table_exists "im_trans_prices"]} {
                    db_0or1row currency "select currency, count(*) as num_prices from im_trans_prices where company_id = :provider_id group by currency order by num_prices desc limit 1"
                }
            }
            set user_id $customer_contact_id
            set company_contact_id $provider_contact_id
            set company_id $provider_id
        }

        if {$currency eq ""} {
            set currency [parameter::get -package_id [im_package_cost_id] -parameter "DefaultCurrency" -default "EUR"]
        }
	
        
        # Info about the customer
        db_1row select_company {
                select  default_tax,
                        default_payment_method_id,
                        payment_term_id,
                        vat_type_id
                from	im_companies c
                where	company_id = :company_id
        }
        set locale [lang::user::locale -user_id $company_contact_id]

        if {$template_id eq ""} {
            set template_id [cog::invoice::template -cost_type_id $invoice_type_id -company_id $company_id -cost_center_id $cost_center_id]
        }


        # Get the payment days from the company
	    set payment_days ""
        if {$payment_term_id ne ""} {
            set payment_days [db_string payment_days "select aux_int1 from im_categories where category_id = :payment_term_id" -default ""]
        }
        if {$payment_days eq ""} {
            set payment_days [parameter::get -package_id [im_package_cost_id] -parameter "DefaultCompanyInvoicePaymentDays" -default "30"]
        }

        if {$payment_method_id eq ""} {
            set payment_method_id $default_payment_method_id
        }
    
        # Set the old default vat to the vat_type_id.
	    set default_vat [db_string vat "select aux_int1 from im_categories where category_id = :vat_type_id" -default ""]

        set tax_format [im_l10n_sql_currency_format -style simple]
	
        set invoice_id ""
        if {$effective_date eq ""} {
            set effective_date "now()"
        }
        
        set invoice_id [im_new_object_id]

        set invoice_nr [im_next_invoice_nr -cost_type_id $invoice_type_id -cost_center_id $cost_center_id]

        set invoice_id [db_exec_plsql create_invoice {
            select im_invoice__new (
                :invoice_id,		-- invoice_id
                'im_invoice',		-- object_type
                now(),			-- creation_date
                :current_user_id,		-- creation_user
                '[ad_conn peeraddr]',	-- creation_ip
                null,			-- context_id
                :invoice_id,		-- invoice_nr
                :customer_id,		-- company_id
                :provider_id,	-- provider_id
                :company_contact_id,	-- company_contact_id
                :effective_date,		    -- invoice_date
                :currency,			-- currency
                :template_id,	-- invoice_template_id
                :invoice_status_id,	-- invoice_status_id
                :invoice_type_id,		-- invoice_type_id
                :payment_method_id,	-- payment_method_id
                :payment_days,		-- payment_days
                0,			    -- amount
                to_number(:default_vat,:tax_format),			-- vat
                to_number(:default_tax,:tax_format),			-- tax
                :note			-- note
            )}]
        
        if {[db_string invoice_nr "select 1 from im_invoices where invoice_nr = :invoice_nr" -default 0]} {
            set invoice_nr [im_next_invoice_nr -cost_type_id $invoice_type_id -cost_center_id $cost_center_id]
        }

        db_dml update_invoice_nr "update im_invoices set invoice_nr = :invoice_nr where invoice_id = :invoice_id"

        db_dml update_costs "
            update im_costs
            set
                project_id	= :project_id,
                cost_name	= :invoice_nr,
                customer_id	= :customer_id,
                cost_nr		= :invoice_id,
                provider_id	= :provider_id,
                cost_status_id	= :invoice_status_id,
                cost_type_id	= :invoice_type_id,
                cost_center_id	= :cost_center_id,
                template_id	= :template_id,
                payment_days	= :payment_days,
                vat		= to_number(:default_vat,:tax_format),
                tax		= to_number(:default_tax,:tax_format),
                variable_cost_p = 't',
                currency	= :currency,
                payment_term_id = :payment_term_id,
                vat_type_id     = :vat_type_id,
                delivery_date = :delivery_date
            where
                cost_id = :invoice_id
        "

        if {$invoice_id ne ""} {
            set invoice_office_id ""
            if {$project_id ne ""} {
                set invoice_office_id [db_string company_accounting_office "select company_office_id from im_projects where project_id = :project_id" -default ""]
            }

            #   If that 'company_office_id' in 'im_projects' is null, we use main_office_id as value
            if {$invoice_office_id eq ""}  {
                set invoice_office_id [db_string company_main_office_info "select main_office_id from im_companies where company_id = :company_id" -default ""]
            }
            db_dml update_invoice_office_id "update im_invoices set invoice_office_id = :invoice_office_id where invoice_id =:invoice_id"
        }

        if {$project_id ne ""} {
            # Add the link between the project and the invoice
            set rel_id [db_exec_plsql create_rel "      select acs_rel__new (
                null,             -- rel_id
                'relationship',   -- rel_type
                :project_id,      -- object_id_one
                :invoice_id,      -- object_id_two
                null,             -- context_id
                null,             -- creation_user
                null             -- creation_ip
            )"]
        }


        return $invoice_id
    }

    ad_proc -public template {
        { -invoice_id "" }
        { -cost_center_id "" }
        { -cost_type_id "" }
        { -company_id "" }
    } {
        Determine the invoice template for a given company.

        @param invoice_id get the default template for a specific invoice
        @param cost_type_id if no invoice_id is provided we need a cost_type to find the best invoice
        @param company_id check the company_id if we have data for the company
        @param cost_center_id check the cost_center if no template was found for the company
    } {
        set template_id ""
        
        # Get the short_name of the cost_type, a string like "invoice"
        # "bill" or "delnote".
        set short_name [db_string cost_type_short_name "
            select  short_name
            from    im_cost_types
            where   cost_type_id = :cost_type_id
            " -default "unknown"]
        set column_name "default_${short_name}_template_id"

        # 1. If cost center is provided, look first if we have a template for this cost center
        if {$cost_center_id ne "" && $template_id eq ""} {
            catch {
                set template_id [db_string cost_type_short_name "
                    select  $column_name
                    from    im_cost_centers, im_categories
                    where   cost_center_id = :cost_center_id
                    and $column_name = category_id
                    and enabled_p = 't'
                " -default ""]
            } err_msg
        } 

        # 2. Default Template For the cost_type_id taken from the company this is used for
        if {$template_id eq ""} {
            catch {
                set template_id [db_string cost_type_short_name "
                    select  $column_name
                    from    im_companies, im_categories
                    where   company_id = :company_id
                    and $column_name = category_id
                    and enabled_p = 't'
                        " -default ""]
            } err_msg
        }

        # 3. Default template of the invoice if invoice_id is provided
        if {$template_id eq ""} {
            if {$invoice_id ne ""} {
                set cost_center_id [db_string cost_center_id_from_invoice "select cost_center_id from im_costs where cost_nr =:invoice_id" -default ""]
                catch {
                    set template_id [db_string cost_type_short_name "
                        select  $column_name
                        from    im_cost_centers, im_categories
                        where   cost_center_id = :cost_center_id
                        and $column_name = category_id
                        and enabled_p = 't'
                    " -default ""]
                } err_msg
            }
        }

        # 4. Default template for cost_type_id taken from the internal (for customers) or freelance_internal (for providers) company.
        if {$template_id eq ""} {
            set company_type_provider [im_company_type_provider]
            set company_type_id [db_string company_type_id "select company_type_id from im_companies where company_id =:company_id" -default ""]

            if {$company_type_id ne ""} {
                if {$company_type_id eq $company_type_provider} {
                    set required_company_id [im_company_freelance]
                } else {
                    set required_company_id [im_company_internal]
                }
                catch {
                    set template_id [db_string cost_type_short_name "
                        select  $column_name
                        from    im_companies, im_categories
                        where   company_id = :company_id
                        and $column_name = category_id
                        and enabled_p = 't'
                            " -default ""]
                } err_msg
            }
        }

        if {$template_id ne ""} {
            cog_log Debug "Found template [im_name_from_id $template_id]"
        }
        # 5. Default template for parent cost_type_id according to im_categories_hierarchy table
        if {$template_id eq ""} {
            # get category parents
            set template_temp_id ""
            set category_parents_ids [im_category_parents $cost_type_id]
            # using reversed list to use hierarchy 'from bottom to top', NOT SURE
            set reversed_category_parent_ids [lreverse $category_parents_ids]
            # first loop to check for actual company
            foreach category_id $reversed_category_parent_ids {
                if {$template_temp_id eq ""} {
                    set short_name [db_string cost_type_short_name "
                        select  short_name
                        from    im_cost_types
                        where   cost_type_id = :category_id
                        " -default ""]
                    if {$short_name ne ""} {
                        set column_name "default_${short_name}_template_id"
                        catch {
                            set template_temp_id [db_string cost_type_short_name "
                                select  $column_name
                                from    im_companies, im_categories
                                where   company_id = :company_id
                                and $column_name = category_id
                                and enabled_p = 't'
                                    " -default ""]
                        } err_msg
                    }
                }
            }
            # second loop to check for internal / freelance_internal company, run only if template_temp_id is still empty
            if {$template_temp_id eq ""} {
                foreach category_id $reversed_category_parent_ids {
                    if {$template_temp_id eq ""} {
                        if {$company_type_id eq $company_type_provider} {
                            set required_company_id [im_company_freelance]
                        } else {
                            set required_company_id [im_company_internal]
                        }
                        set short_name [db_string cost_type_short_name "
                            select  short_name
                            from    im_cost_types
                            where   cost_type_id = :category_id
                            " -default ""]
                        if {$short_name ne ""} {
                            set column_name "default_${short_name}_template_id"
                            catch {
                                set template_temp_id [db_string cost_type_short_name "
                                    select  $column_name
                                    from    im_companies, im_categories
                                    where   company_id = :company_id
                                    and $column_name = category_id
                                    and enabled_p = 't'
                                        " -default ""]
                            } err_msg
                        }
                    }
                }
            }
            set template_id $template_temp_id
        }
        return $template_id
    }

     ad_proc oo_document {
        -invoice_id 
        { -user_id ""}
    } {
        Returns the parsed fodt document for the file

        Does not support ODT
    } {

        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }

        # ---------------------------------------------------------------
        # Check permissions
        # ---------------------------------------------------------------
        set perm_proc [im_parameter -package_id [im_package_invoices_id] "InvoicePermissionProc"]

        $perm_proc $user_id $invoice_id view read write admin

        if {!$read} {
            ad_return_complaint "[lang::message::lookup $user_locale intranet-invoices.lt_Insufficient_Privileg]" "
            <li>[lang::message::lookup $user_locale intranet-invoices.lt_You_have_insufficient_1]<BR>
            [lang::message::lookup $user_locale intranet-invoices.lt_Please_contact_your_s]"
            ad_script_abort
        }

        set cost_type_id [db_string cost_type_id "select cost_type_id from im_costs where cost_id = :invoice_id" -default 0]
        if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
            set recipient_select "ci.customer_id as recipient_company_id"
            set customer_or_provider_join "and ci.customer_id = c.company_id"
        } else {
            set customer_or_provider_join "and ci.provider_id = c.company_id"
            set recipient_select "ci.provider_id as recipient_company_id"
        }

        set invoice_template_base_path [im_parameter -package_id [im_package_invoices_id] InvoiceTemplatePathUnix "" "/tmp/templates/"]
        set material_enabled_p [im_parameter -package_id [im_package_invoices_id] "ShowInvoiceItemMaterialFieldP" "" 0]

        # ---------------------------------------------------------------
        # Get everything about the invoice
        # ---------------------------------------------------------------

        set query "
            select
                c.*,
                i.*,
                now()::date as todays_date,
                        $recipient_select ,
                ci.effective_date::date + ci.payment_days AS due_date,
                ci.effective_date AS invoice_date,
                ci.cost_status_id AS invoice_status_id,
                ci.cost_type_id AS invoice_type_id,
                ci.template_id AS invoice_template_id,
                ci.*,
                ci.note as cost_note,
                ci.project_id as cost_project_id,
                to_date(to_char(ci.effective_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') + ci.payment_days as calculated_due_date,
                im_cost_center_name_from_id(ci.cost_center_id) as cost_center_name,
                im_category_from_id(ci.cost_status_id) as cost_status,
                im_category_from_id(ci.cost_type_id) as cost_type,
                im_category_from_id(c.default_payment_method_id) as default_payment_method,
                im_category_from_id(ci.template_id) as template,
                im_category_from_id(c.company_type_id) as company_type,
                im_category_from_id(c.vat_type_id) as company_vat_type,
                im_category_from_id(ci.vat_type_id) as invoice_vat_type,
                im_category_from_id(ci.template_id) as invoice_template
            from
                im_invoices i,
                im_costs ci,
                im_companies c
            where
                i.invoice_id=:invoice_id
                and ci.cost_id = i.invoice_id
                $customer_or_provider_join
        "

        if { ![db_0or1row invoice_info_query $query] } {
            # We couldn't get the base information for this invoice.
            # fraber 151210: This happened today with an invoice with
            # a deleted customer company. No idea how that could happen...
            ad_return_complaint 1 [lang::message::lookup en_US intranet-invoices.Unable_to_get_invoice_info_inconsistent_data "We are unable to get the invoice information for this object. This should never happen. In the past this happened once, after deleting the customer company of an invoice."]
            ad_script_abort
        }

        if {$template_id eq ""} {
            set template_id [cog::invoice::template -cost_type_id $invoice_type_id -company_id $customer_id -cost_center_id $cost_center_id]
        } 

        if {$template_id eq ""} {
            return ""
            cog_log Error "can't find a template for $invoice_id [im_name_from_id $invoice_id]"
            ad_script_abort
        }

        set template [im_category_from_id -translate_p 0 $template_id]
        set template_type ""

        set recipient_locale ""
        if {[regexp {(.*)\.([_a-zA-Z]*)\.([a-zA-Z][a-zA-Z][a-zA-Z])} $template match body loc template_type]} {
    	    set recipient_locale $loc
        }

        # Check if the given locale throws an error
        # Reset the locale to the default locale then
        if {[catch {
            lang::message::lookup $recipient_locale "intranet-core.Reporting"
        } errmsg]} {
            set recipient_locale [lang::user::locale -user_id $company_contact_id]
        }

        if {"" != $recipient_locale} {
            set locale $recipient_locale
        } else {
            set locale [lang::user::locale -user_id $user_id]
        }

        set template_type [string tolower $template_type]

        # Handle fodt
        if {$template_type eq "fod"} {set template_type "fodt"}


        # ---------------------------------------------------------------
        # Find out if the invoice is associated with a _single_ project
        # or with more then one project. Only in the case of exactly one
        # project we can access the "customer_project_nr" for the invoice.
        # ---------------------------------------------------------------

        set related_projects_sql "
                select distinct
                r.object_id_one as project_id,
                p.project_name,
                        im_name_from_id(p.project_lead_id) as project_manager,
                p.project_nr,
                p.parent_id,
                p.description,
                trim(both p.company_project_nr) as customer_project_nr,
                main_p.project_id as main_project_id,
                main_p.project_nr as main_project_nr,
                main_p.project_name as main_project_name
            from
                    acs_rels r,
                im_projects p,
                im_projects main_p
            where
                r.object_id_one = p.project_id and
                    r.object_id_two = :invoice_id and
                tree_root_key(p.tree_sortkey) = main_p.tree_sortkey
        "

        set related_projects {}
        set related_project_nrs {}
        set related_project_names {}
        set related_project_descriptions ""
        set related_customer_project_nrs {}

        set num_related_projects 0
        db_foreach related_projects $related_projects_sql {
            lappend related_projects $project_id
            if {"" != $project_nr} {
                lappend related_project_nrs $project_nr
            }
            if {"" != $project_name} {
                lappend related_project_names $project_name
            }
            
            if {"" != $description && 0 == $num_related_projects} {
                append related_project_descriptions $description
            } else {
                append related_project_descriptions ", $description"
            }

            set main_project_nr [string trim $main_project_nr]
            set main_project_name [string trim $main_project_name]
            set related_main_projects_hash($main_project_id) $main_project_id
            set related_main_project_nrs_hash($main_project_nr) $main_project_nr
            set related_main_project_names_hash($main_project_name) $main_project_name

            # Check of the "customer project nr" of the superproject, as the PMs
            # are probably too lazy to maintain it in the subprojects...
            set cnt 0
            while {"" eq $customer_project_nr && "" ne $parent_id && $cnt < 10} {
                set customer_project_nr [db_string custpn "select company_project_nr from im_projects where project_id = :parent_id" -default ""]
                set parent_id [db_string parentid "select parent_id from im_projects where project_id = :parent_id" -default ""]
                incr cnt
            }
            if {"" != $customer_project_nr} {
                lappend related_customer_project_nrs $customer_project_nr
            }
            incr num_related_projects
        }

        set rel_project_id 0
        if {1 == [llength $related_projects]} {
            set rel_project_id [lindex $related_projects 0]
            set related_project_names [lindex $related_project_names 0]
        }

        set related_main_projects [lsort [array names related_main_projects_hash]]
        set related_main_project_nrs [lsort [array names related_main_project_nrs_hash]]
        set related_main_project_names [lsort [array names related_main_project_names_hash]]
        if {1 == [llength $related_main_projects]} {
            set related_main_project_nrs [lindex $related_main_project_nrs 0]
            set related_main_project_names [lindex $related_main_project_names 0]
        }

        # ---------------------------------------------------------------
        # Get everything about the contact person.
        # ---------------------------------------------------------------

        # Make sure to unset the company name if the company is a freelancer

        if {[string match "Freelance*" $company_name]} {
            set company_name_pretty ""
        } else {
            set company_name_pretty $company_name
        }

        # Use the "company_contact_id" of the invoices as the main contact.
        # Fallback to the accounting_contact_id and primary_contact_id
        # if not present.

        if { ![info exists company_contact_id] } { set company_contact_id ""}

        set company_contact_orig $company_contact_id

        if {"" == $company_contact_id} {
            set company_contact_id $accounting_contact_id
        }
        if {"" == $company_contact_id} {
            set company_contact_id $primary_contact_id
        }
        set org_company_contact_id $company_contact_id

        set company_contact_name ""
        set company_contact_email ""
        set company_contact_first_names ""
        set company_contact_last_name ""

        db_0or1row accounting_contact_info "
            select
                im_name_from_user_id(person_id) as company_contact_name,
                im_email_from_user_id(person_id) as company_contact_email,
                first_names as company_contact_first_names,
                last_name as company_contact_last_name,
                salutation_id
            from	persons
            where	person_id = :company_contact_id
        "

        # If the company_contact_id is not maintained, write it now
        if {$company_contact_orig eq ""} {
            db_dml update_company_contact "update im_invoices set company_contact_id = :company_contact_id where invoice_id = :invoice_id"
        }

        set salutation_pretty "[im_invoice_salutation -person_id $company_contact_id],"

        # Set these fields if contacts is not installed:
        if {![info exists salutation]} { set salutation "" }
        if {![info exists user_position]} { set user_position "" }

        # Get contact person's contact information
        set contact_person_work_phone ""
        set contact_person_work_fax ""
        set contact_person_email ""
        db_0or1row contact_info "
            select
                work_phone as contact_person_work_phone,
                fax as contact_person_work_fax,
                im_email_from_user_id(user_id) as contact_person_email
            from
                users_contact
            where
                user_id = :company_contact_id
        "
        # ----------------------------------------------------------------------------------------
        # Check if there are Dynamic Fields of type date and localize them
        # ----------------------------------------------------------------------------------------

        set date_fields [list]
        set column_sql "
                select  w.widget_name,
                        aa.attribute_name
                from    im_dynfield_widgets w,
                        im_dynfield_attributes a,
                        acs_attributes aa
                where   a.widget_name = w.widget_name and
                        a.acs_attribute_id = aa.attribute_id and
                        aa.object_type = 'im_invoice' and
                        w.widget_name = 'date'
        "
        db_foreach column_list_sql $column_sql {
            set y ${attribute_name}
            set z [lc_time_fmt [subst $${y}] "%x" $locale]
            set ${attribute_name} $z
        }

        # Special ODT functionality: We need to parse the ODT template
        # in order to extract the table row that needs to be formatted
        # by the loop below.
    
        # ------------------------------------------------
        # Read the FODT file, it is XML
        set invoice_template_path "$invoice_template_base_path/$template"
        if {![file exists $invoice_template_path]} {
            return ""
            ad_script_abort
        }

        cog_log Debug "File... $invoice_template_path"
        set file [open "$invoice_template_path"]
        fconfigure $file -encoding "utf-8"
        set odt_template_content [read $file]
        set odt_file "[ad_tmpnam].fodt"
        close $file

        # ------------------------------------------------
        # Search the <row> ...<cell>..</cell>.. </row> line
        # representing the part of the template that needs to
        # be repeated for every template.

        # Get the list of all "tables" in the document
        set odt_doc [dom parse $odt_template_content]
        set root [$odt_doc documentElement]
        set odt_table_nodes [$root selectNodes "//table:table"]

        # Search for the table that contains "@item_name"
        set odt_template_table_node ""

        foreach table_node $odt_table_nodes {
            set table_as_list [$table_node asList]
            if {[regexp {item_name} $table_as_list match]} { set odt_template_table_node $table_node }
        }

        # Deal with the the situation that we didn't find the line
        if {"" == $odt_template_table_node} {
            ad_return_complaint 1 "
                <b>Didn't find table including '@item_name'</b>:<br>
                We have found a valid OOoo template at '$invoice_template_path'.
                However, this template does not include a table with the value
                above.
            "
            ad_script_abort
        }

        # Search for the 2nd table:table-row tag
        set odt_table_rows_nodes [$odt_template_table_node selectNodes "//table:table-row"]
        set odt_template_row_node ""
        set odt_template_row_count 0
        foreach row_node $odt_table_rows_nodes {
        	set row_as_list [$row_node asList]
            if {[regexp {item_name} $row_as_list match]} { set odt_template_row_node $row_node }
            incr odt_template_row_count
        }

        if {"" == $odt_template_row_node} {
            ad_return_complaint 1 "
                <b>Didn't find row including '@item_name'</b>:<br>
                We have found a valid OOoo template at '$invoice_template_path'.
                However, this template does not include a row with the value
                above.
            "
            ad_script_abort
        }

        # Convert the tDom tree into XML for rendering
        set odt_row_template_xml [$odt_template_row_node asXML]

        # ---------------------------------------------------------------
        # Format Invoice date information according to locale
        # ---------------------------------------------------------------

        set invoice_date_pretty [lc_time_fmt $invoice_date "%x" $locale]
        if {[catch {set delivery_date_pretty [lc_time_fmt $delivery_date "%x" $recipient_locale]}]} {
            set delivery_date_pretty $delivery_date
        }
        set delivery_date_pretty2 $delivery_date_pretty

        set calculated_due_date_pretty [lc_time_fmt $calculated_due_date "%x" $locale]
        set todays_date_pretty [lc_time_fmt $todays_date "%x" $locale]

        # ---------------------------------------------------------------
        # Get more about the invoice's project
        # ---------------------------------------------------------------

        # We give priority to the project specified in the cost item,
        # instead of associated projects.
        if {"" != $cost_project_id && 0 != $cost_project_id} {
            set rel_project_id $cost_project_id
        }

        set project_type_id ""

        db_0or1row project_info_query "
                select
                    project_nr as project_short_name,
                    project_type_id,
                    start_date,
                    end_date,
                    processing_time,
                    company_project_nr
                from
                    im_projects
                where
                    project_id = :rel_project_id
        "
        
        set project_type_pretty [im_category_from_id -current_user_id $user_id -locale $recipient_locale $project_type_id]

        # Skills
        # Append the skill information to the multirow
        if {[apm_package_installed_p "intranet-freelance"]} {
            # Initialize skills
            db_foreach skill_type "select category_id from im_categories where category_type = 'Intranet Skill Type'" {
                set skill_${category_id}_pretty ""
            }
            
            # Get the project_skills
            db_foreach skills_for_object "select skill_type_id, skill_id,aux_string2 from im_object_freelance_skill_map, im_categories where object_id = :project_id and category_id = skill_type_id order by sort_order " {
                set skill_${skill_type_id}_pretty "[im_category_from_id -current_user_id $user_id $skill_id]"
            }
        }

        set query "
        select
                pm_cat.category as invoice_payment_method,
            pm_cat.category_description as invoice_payment_method_desc
        from
                im_categories pm_cat
        where
                pm_cat.category_id = :payment_method_id
        "
        if { ![db_0or1row category_info_query $query] } {
            set invoice_payment_method ""
            set invoice_payment_method_desc ""
        }

        set invoice_payment_method_l10n $invoice_payment_method
        set invoice_payment_method_key [lang::util::suggest_key $invoice_payment_method]
        if {"" ne $invoice_payment_method_key} {
            set invoice_payment_method_l10n [lang::message::lookup $locale intranet-core.$invoice_payment_method_key $invoice_payment_method]
        }

        # Fallback for empty office_id: Main Office
        if {"" == $invoice_office_id} {
            set invoice_office_id $main_office_id
        }

        db_1row office_info_query "
            select *
            from im_offices
            where office_id = :invoice_office_id
        "

        # ---------------------------------------------------------------
        # Determine the country name and localize
        # ---------------------------------------------------------------

        set country_name ""
        if {"" != $address_country_code} {
            set query "
            select	cc.country_name
            from	country_codes cc
            where	cc.iso = :address_country_code"
            if { ![db_0or1row country_info_query $query] } {
                set country_name $address_country_code
            }
            set country_name [lang::message::lookup $locale intranet-core.$country_name $country_name]
        }

        # Number formats
        set cur_format [im_l10n_sql_currency_format -style separators]

        # Rounding precision can be between 2 (USD,EUR, ...) and -5 (Old Turkish Lira, ...).
        set rf [im_invoice_rounding_factor -currency $currency]
        set rounding_precision 2
        set ctr 0

        if [string match $odt_row_template_xml "@item_description;noquote@"] {
            set description_included_p 1
        } else {
            set description_included_p 0
        }

        db_foreach invoice_items {
            select
                i.description as item_description,
                i.*,
                i.item_type_id,
                i.item_uom_id,
                    round(coalesce(i.price_per_unit * i.item_units , 0) * :rf) / :rf as amount,
                    i.currency as item_currency,
                    i.sort_order as item_sort_order
            from
                im_invoice_items i
            where
                i.invoice_id=:invoice_id
                order by
                i.sort_order,
                i.item_type_id
        } {
            
            incr ctr
            set amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $amount+0] $rounding_precision] "" $locale]
            set item_units_pretty [lc_numeric [expr $item_units+0] "" $locale]
            set price_per_unit_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $price_per_unit+0] $rounding_precision] "" $recipient_locale]
            set item_uom  [im_category_from_id -current_user_id $user_id -locale $recipient_locale $item_uom_id]
            set item_type [im_category_from_id -current_user_id $user_id -locale $recipient_locale $item_type_id]
            
            if {$item_description ne "" && !$description_included_p} {
                set item_name "$item_name<br /> $item_description"
            }

            # Display the material if we have materials enabled for invoice line items
            if {$material_enabled_p} {
                if {"" != $item_material_id && 12812 != $item_material_id} {
                    set folder_proc [parameter::get -package_id [im_package_material_id] -parameter "MaterialNamePrettyProc" -default "im_material_name_helper"]
                    if {$folder_proc eq "im_material_name_helper"} {
                                set item_material [$folder_proc -material_id $item_material_id]
                    } else {
                            set item_material [$folder_proc -material_id $item_material_id -user_id $user_id]
                    }
                } else {
                    set item_material ""
                }
            }

            # If we have a material based taxation, add the VAT now
            set item_vat $vat
            set line_item_vat_ids [list]
            if {$vat_type_id == 42021} {            
                set item_vat [db_string vat {
                    select coalesce(ct.aux_num1,0) as vat
                    from im_categories cm, im_categories ct, im_invoice_items ii, im_materials im
                    where cm.aux_int2 = ct.category_id
                    and ii.item_material_id = im.material_id
                    and im.material_type_id = cm.category_id
                    and ii.item_id = :item_id
                } -default ""]
                
                if {$item_vat ne ""} {
                    append invoice_item_html " (${item_vat}% VAT)"
                    set item_vat_pretty "${item_vat}%"
                    if {[lsearch $line_item_vat_ids $item_vat]<0} {
                        lappend line_item_vat_ids $item_vat
                    }
                }
            }
            
            set amount_vat_pretty ""
            if {$amount >0 && $item_vat >0} {
                set amount_vat_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr {double(round($amount+$amount*$item_vat/100))}] $rounding_precision] "" $recipient_locale]
            }
                        
            # Insert a new XML table row into OpenOffice document
            eval [template::adp_compile -string $odt_row_template_xml]
            set odt_row_xml $__adp_output

            # Parse the new row and insert into OOoo document
            set row_doc [dom parse -simple [intranet_oo::convert -content $odt_row_xml]]
            set new_row [$row_doc documentElement]
            $odt_template_table_node insertBefore $new_row $odt_template_row_node

            incr ct
        }

        # Handle processing time. 
        if {$processing_time ne 0} {
            set delivery_time $processing_time
        } else {

            if {$start_date ne "" && $end_date ne ""} {
                set delivery_time [im_absence_calculate_absence_days -start_date $start_date -end_date $end_date -group_ids [im_employee_group_id]]
            } else {
                set delivery_time 0
            }
        }
        if {$delivery_time <1} {
            set delivery_time "[_ cognovis-core.TBD]"
        }
        # ---------------------------------------------------------------
        # Source Invoices list
        # ---------------------------------------------------------------

        set linked_invoice_ids [im_invoices::linked_invoices -invoice_id $invoice_id]

        if {$linked_invoice_ids ne ""} {
            
            set linked_list_sql "
                select
                    invoice_id as linked_invoice_id,
                        invoice_nr as linked_invoice_nr,
                        effective_date as linked_effective_date,
                        cost_type_id as linked_cost_type_id
                from
                    im_invoices, im_costs
                where
                    invoice_id in ([template::util::tcl_to_sql_list $linked_invoice_ids])
                        and cost_id = invoice_id
            "
            
            set linked_ctr 0
            db_foreach linked_list $linked_list_sql {
                set linked_invoice_nr_$linked_cost_type_id $linked_invoice_nr
                set linked_effective_date_pretty [lc_time_fmt $linked_effective_date "%x" $locale]
                set linked_effective_date_pretty_$linked_cost_type_id $linked_effective_date_pretty
            }            
        }

        if {[im_column_exists im_costs vat_type_id]} {
            # get the VAT note. We do not overwrite the VAT value stored in
            # the invoice in case the default rate has changed for the
            # vat_type_id and this is just a reprint of the invoice
            set vat_note [im_category_string1 -category_id $vat_type_id -locale $recipient_locale]
        } else {
            set vat_note ""
        }

        # -------------------------
        # Deal with payment terms and variables in them
        # -------------------------

        if {"" == $payment_term_id} {
            set payment_term_id [db_string payment_term "select payment_term_id from im_companies where company_id = :recipient_company_id" -default ""]
        }
        set payment_terms [im_category_from_id -current_user_id $user_id -locale $recipient_locale $payment_term_id]
        set payment_terms_note [im_category_string1 -category_id $payment_term_id -locale $recipient_locale]
        eval [template::adp_compile -string $payment_terms_note]
        set payment_terms_note $__adp_output

        if {$payment_terms_note eq ""} { set payment_terms_note $payment_terms }

        # -------------------------
        # Deal with payment method and variables in them
        # -------------------------

        if {$payment_method_id eq "" && $default_payment_method_id ne ""} {
            set payment_method_id $default_payment_method_id
            db_dml update_payment_method "update im_invoices set payment_method_id = :payment_method_id where invoice_id = :invoice_id"
        }
        set payment_method [im_category_from_id -current_user_id $user_id -locale $recipient_locale $payment_method_id]
        set payment_method_note [im_category_string1 -category_id $payment_method_id -locale $recipient_locale]
        eval [template::adp_compile -string $payment_method_note]
        set payment_method_note $__adp_output

        if {$payment_method_note eq ""} { set payment_method_note $payment_method }

        # -------------------------------
        # Support for cost center text
        # -------------------------------
        set cost_center_note [lang::message::lookup $recipient_locale intranet-cost.cc_invoice_text_${cost_center_id} " "]

        # Set these values to 0 in order to allow to calculate the
        # formatted grand total
        if {"" == $vat} { set vat 0}
        if {"" == $tax} { set tax 0}

        db_1row grand_total {
           	select	i.*,
                round(i.grand_total * :vat / 100 * :rf) / :rf as vat_amount,
                round(i.grand_total * :tax / 100 * :rf) / :rf as tax_amount,
                i.grand_total
                    + round(i.grand_total * :vat / 100 * :rf) / :rf
                    + round(i.grand_total * :tax / 100 * :rf) / :rf
                as total_due
            from
                (select
                    max(i.currency) as currency,
                    sum(i.amount) as subtotal,
                    round(sum(i.amount) * :surcharge_perc::numeric) / 100.0 as surcharge_amount,
                    round(sum(i.amount) * :discount_perc::numeric) / 100.0 as discount_amount,
                    sum(i.amount)
                        + round(sum(i.amount) * :surcharge_perc::numeric) / 100.0
                        + round(sum(i.amount) * :discount_perc::numeric) / 100.0
                    as grand_total
                from 
                    (select	ii.*,
                        round(ii.price_per_unit * ii.item_units * :rf) / :rf as amount
                    from	im_invoice_items ii,
                        im_invoices i
                    where	i.invoice_id = ii.invoice_id
                        and i.invoice_id = :invoice_id
                    ) i
                ) i
        }

        # Overwrite for material based calculation
        if {$vat_type_id == 42021} {
            set vat_amount [db_string total "select coalesce(sum(round(item_units*price_per_unit*cb.aux_num1/100,2)),0)
                                                        from im_invoice_items ii, im_categories ca, im_categories cb, im_materials im
                                                        where invoice_id = :invoice_id
                                                        and ca.category_id = material_type_id
                                                        and ii.item_material_id = im.material_id
                                                        and ca.aux_int2 = cb.category_id"
                ]
            if {$vat_amount ne "0.00" && $subtotal ne "0" && $subtotal ne "" } {
            set vat [format "%.2f" [expr $vat_amount / $subtotal *100]]
            set total_due [expr $vat_amount + $subtotal]
            }
        }

        set subtotal_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $subtotal+0] $rounding_precision] "" $locale]
        set vat_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $vat_amount+0] $rounding_precision] "" $locale]
        set tax_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $tax_amount+0] $rounding_precision] "" $locale]

        set vat_perc_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $vat+0] $rounding_precision] "" $locale]
        set tax_perc_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $tax+0] $rounding_precision] "" $locale]
        set grand_total_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $grand_total+0] $rounding_precision] "" $locale]
        set total_due_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $total_due+0] $rounding_precision] "" $locale]

        set discount_perc_pretty $discount_perc
        set surcharge_perc_pretty $surcharge_perc

        set discount_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $discount_amount+0] $rounding_precision] "" $locale]
        set surcharge_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $surcharge_amount+0] $rounding_precision] "" $locale]

        ##---------------------------------------------------------------
        # Handle material based vats
        #---------------------------------------------------------------
        
        # Initialize the various vat_amounts
        foreach vat_id [db_list vat_ids "select distinct aux_num1 from im_categories where category_type = 'Intranet VAT Type'"] {
            set vat_amount_${vat_id} ""
        }

        if {"" != $vat && 0 != $vat} {
            set vat_amount_total 0
            if {[llength $line_item_vat_ids]>0} {
                foreach vat_id $line_item_vat_ids {
	                set vat_amount [db_string vat_amount "select coalesce(sum(round(item_units*price_per_unit*cb.aux_num1/100,2)),0) as vat_amount
                                                         from im_invoice_items ii, im_categories ca, im_categories cb, im_materials im
                                                        where invoice_id = :invoice_id
                                                          and ca.category_id = material_type_id
                                                          and ii.item_material_id = im.material_id
                                                          and ca.aux_int2 = cb.category_id
                                                          and cb.aux_num1 = :vat_id" -default ""]
                    set vat_amount_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $vat_amount+0] $rounding_precision] "" $recipient_locale]
                    set vat_perc_pretty [lc_numeric [im_numeric_add_trailing_zeros [expr $vat_id+0] $rounding_precision] "" $recipient_locale]
            	    set vat_amount_total [expr $vat_amount_total + $vat_amount]
	        	    set vat_amount_${vat_id} "$vat_amount_pretty"
                }
                # Store the total vat amount with the cost
                db_dml update_cost "update im_costs set vat_amount = :vat_amount_total where cost_id = :invoice_id"
	        }
        }

        # Delete the original template row, which is duplicate
        $odt_template_table_node removeChild $odt_template_row_node

        # Process the content.xml file
        set odt_template_content [$root asXML -indent 1]

        # Escaping other vars used, skip vars already escaped for multiple lines
        set lines [split $odt_template_content \n]
        set vars_already_escaped {item_name item_units_pretty item_uom price_per_unit amount_formatted}

        foreach line $lines {
            set var_to_be_escaped ""
            regexp -nocase {@(.*?)@} $line var_to_be_escaped
            regsub -all "@" $var_to_be_escaped {} var_to_be_escaped
            regsub -all ";noquote" $var_to_be_escaped {} var_to_be_escaped
            if { -1 == [lsearch $vars_already_escaped $var_to_be_escaped] } {
                if { "" != $var_to_be_escaped  } {
                    if { [info exists $var_to_be_escaped] } {
                        set value [set $var_to_be_escaped]
                        set $var_to_be_escaped [encodeXmlValue $value]
                        lappend vars_already_escaped $var_to_be_escaped
                    }
                }
            } else {
            }
        }

        # Perform replacements
        regsub -all "&lt;%" $odt_template_content {<%} odt_template_content
        regsub -all "%&gt;" $odt_template_content {%>} odt_template_content

        # ------------------------------------------------
        # Rendering
        #
        
        callback im_invoices::oo::before_render -cost_type_id $cost_type_id -invoice_id $invoice_id -current_user_id $user_id

        if {[catch {
        eval [template::adp_compile -string $odt_template_content]
        } err_msg]} {
            set err_info $::errorInfo
            set err_txt [lang::message::lookup "" intranet-invoices.Error_rendering_template_blurb "Error rendering Template. You might have used a placeholder that is not available. Here's a detailed error message:"]
            append err_txt "<br/><br/> <strong>[ns_quotehtml $err_msg]</strong><br/>&nbsp;<br/><pre>[ns_quotehtml $err_info]</pre>"
            append err_txt [lang::message::lookup "" intranet-invoices.Check_the_config_manual_blurb "Please check the configuration manual for a list of placeholders available and more information on configuring templates:"]
            append err_txt "<br>&nbsp;<br><a href='www.project-open.com/en/'>www.project-open.com/en/</a>"
            ad_return_complaint 1 [lang::message::lookup "" intranet-invoices $err_txt]
            ad_script_abort
        }

        set content $__adp_output

        # Save the content to a file.
        set file [open $odt_file w]
        fconfigure $file -encoding "utf-8"
        puts $file "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n[intranet_oo::convert -content $content]"
        flush $file
        close $file
        return $odt_file
    }
}



ad_proc -public cog_cost_permissions {user_id cost_id view_var read_var write_var admin_var} {
    Fill the permissions on the cost item.
} {
    upvar $view_var view
    upvar $read_var read
    upvar $write_var write
    upvar $admin_var admin

    set user_is_freelance_p [im_user_is_freelance_p $user_id]
    set user_is_inco_customer_p [im_user_is_inco_customer_p $user_id]
    set user_is_customer_p [im_user_is_customer_p $user_id]

    # -----------------------------------------------------
    # Get Cost information
    set customer_id 0
    set provider_id 0
    set cost_center_id 0
    set cost_type_id 0
    db_0or1row get_companies "
        select	c.customer_id,
            c.provider_id,
            (select o.object_type from acs_objects o where o.object_id = c.provider_id) as provider_otype,
            c.cost_center_id,
            c.cost_type_id
        from	im_costs c
        where	c.cost_id = :cost_id
    "

    # -----------------------------------------------------
    # Cost Center permissions - check if the user has read permissions
    # for this particular cost center
    if {[apm_package_installed_p "intranet-cost-center"]} {
        db_1row cost_type_privileges "select read_privilege, write_privilege from im_cost_types where cost_type_id = :cost_type_id"
        set cc_read [permission::permission_p -no_login -object_id $cost_center_id -party_id $user_id -privilege $read_privilege]
        set cc_write [permission::permission_p -no_login -object_id $cost_center_id -party_id $user_id -privilege $write_privilege]
    } else {
        set cc_read 1
        set cc_write 1
    }

    set can_read [expr [im_permission $user_id view_costs] || [im_permission $user_id view_invoices]]
    set can_write [expr [im_permission $user_id add_costs] || [im_permission $user_id add_invoices]]

    # AND-connection with add/view - costs/invoices
    if {!$can_read} { set cc_read 0 }
    if {!$can_write} { set cc_write 0 }

    # Set the other two variables
    set cc_admin $cc_write
    set cc_view $cc_read

    # -----------------------------------------------------
    # Customers get the right to see _their_ invoices
    set cust_view 0
    set cust_read 0
    set cust_write 0
    set cust_admin 0
    set incust_view 0
    set incust_read 0
    set incust_write 0
    set incust_admin 0

    if {$user_is_inco_customer_p && $customer_id && $customer_id != [im_company_internal]} {
	    im_company_permissions $user_id $customer_id incust_view incust_read incust_write incust_admin
    }
    if {$user_is_customer_p && $customer_id && $customer_id != [im_company_internal]} {
	    im_company_permissions $user_id $customer_id cust_view cust_read cust_write cust_admin
    }

    # -----------------------------------------------------
    # Providers get the right to see _their_ invoices
    # This leads to the fact that FreelanceManagers (the guys
    # who can convert themselves into freelancers) can also
    # see the freelancer's permissions. Is this desired?
    # I guess yes, even if they don't usually have the permission
    # to see finance.
    set prov_view 0
    set prov_read 0
    set prov_write 0
    set prov_admin 0

    switch $provider_otype {
        im_company {
	        if {$user_is_freelance_p && $provider_id && $provider_id != [im_company_internal]} {
                im_company_permissions $user_id $provider_id prov_view prov_read prov_write prov_admin
            }
        }
        user {
            # This is an expense or an expense bundle, probably.
            if {$provider_id eq $user_id} {
                set prov_view 1
                set prov_read 1
            }
        }
    }


    # -----------------------------------------------------
    # Set the permission as the OR-conjunction of provider and customer
    set view [expr {$incust_view || $cust_view || $prov_view || $cc_view}]
    set read [expr {$incust_read || $cust_read || $prov_read || $cc_read}]
    set write [expr {$incust_write || $cust_write || $prov_write || $cc_write}]
    set admin [expr {$incust_admin || $cust_admin || $prov_admin || $cc_admin}]

    # Limit rights of all users to view & read if they dont
    # have the expressive permission to "add_costs or add_invoices".
    if {!$can_write} {
        set write 0
        set admin 0
    }
}
