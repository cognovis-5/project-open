ad_library {

     Slack integration package
         
    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @creation-date 2018-02-02
}

namespace eval cog::slack {
    ad_proc post {
        {-webhook_url ""}
        {-payload ""}
        {-channel ""}
        {-username ""}
        {-icon ""}
        {-text ""}
    } {
        Post a new slack message to the webhook_url using https post
        
        @param webhook_url URL for the webhook
        @param channel Channel into which to post to
        @param username Name of the user used for posting
        @param icon Icon to use for the postin
        @param payload In case we already have the payload.
        @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    } {
        
        if {$webhook_url eq ""} {
            set webhook_url [parameter::get_from_package_key -package_key "cognovis-core" -parameter SlackWebhookURL]
        }
        
        if {$webhook_url eq ""} {
            cog_log Warning "Missing Slack integration"
            return 0
        } else {

            if {$payload eq ""} {
                if {$username eq ""} {
                    set username "[ns_info server]"
                }
                if {$channel eq ""} {
                    set channel "#general"
                }
                set payload "\{\"channel\": \"$channel\", \"icon_emoji\": \"$icon\",
                    \"username\": \"$username\",\"text\": \"[im_quotejson $text]\"
                    \}"
            }

            # set webhook_url "https://duckduckgo.com"
            set post_data "[ns_urlencode "payload"]=[ns_urlencode $payload]"
            set requestHeaders [ns_set create]
            set replyHeaders [ns_set create]
            ns_set update $requestHeaders "Content-type" "application/x-www-form-urlencoded"
            set h [ns_http queue -method POST \
                -headers $requestHeaders \
                -timeout 10.0 \
                -body $post_data $webhook_url]
            
            set r [ns_http wait $h]
            #######################
            # output results
            #######################
            if {[dict get $r status] ne 200} {
                ns_log Error "Slack Error: [dict get $r body] for payload $post_data"
                return 0
            }
            return 1
        }
    }

    ad_proc post_with_attachment {
        {-webhook_url ""}
        {-channel ""}
        {-username ""}
        {-icon ""}
        -fallback
        {-pretext ""}
        {-color "#D00000"}
        {-value ""}
    } {
        Post a new slack message to the webhook_url using https post

        @param webhook_url URL for the webhook
        @param channel Channel into which to post to
        @param username Name of the user used for posting
        @param icon Icon to use for the postin

        @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    } {

        if {$username eq ""} {
            set username "[ns_info server]"
        }
        if {$channel eq ""} {
            set channel "#general"
        }
        set payload "\{\"channel\": \"$channel\", \"icon_emoji\": \"$icon\",
            \"username\": \"$username\",
            \"attachments\": \[ \{
                    \"fallback\":\"[im_quotejson $fallback]\",
                    \"pretext\": \"[im_quotejson $pretext]\",
                    \"color\":\"$color\",
                    \"fields\":\[\{
                        \"value\":\"[im_quotejson $value]\",
                        \"short\":\"false\"
                    \}\]
                \}\]
            \}"
        
        post -payload $payload
    }

    ad_proc post_error {
        -message:required
        {-user_id ""}
    } {
        Post an error to slack
    } {

        set slack_icon [parameter::get_from_package_key -package_key "cognovis-core" -parameter ErrorIcon]
        set slack_channel [parameter::get_from_package_key -package_key "cognovis-core" -parameter ErrorChannel]
        if {$user_id eq ""} {
            set username [parameter::get_from_package_key -package_key "cognovis-core" -parameter ErrorUser]
        } else {
            set username [im_name_from_id $user_id]
        }
        set date [template::util::date::get_property display_date [template::util::date::now]]
        set fallback "$date: $message"
        set pretext  "*$date*: [ad_url][util_get_current_url]"
        return [post_with_attachment -channel $slack_channel -icon $slack_icon -username $username \
            -fallback $fallback \
            -pretext $pretext \
            -color "#D00000" \
            -value $message]
    }
}

