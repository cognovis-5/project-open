namespace eval cog::note {
    ad_proc -public add {
        {-user_id ""}
        -note:required
        -object_id:required
        { -note_type_id "" }
        { -note_status_id "" }
    } {
        Add a note to the object

        @param object_id object *::read Object we want to attach the note to
        @param note string HTML of the note
        @param note_type_id category "Intranet Notes Type" What kind of note are we creating. Will default to default
        @param note_status_id category "Intranet Notes Status" What status is the note going to be in. Typically active. Set to deleted instead of delete endpoint (unless you want to purge)

        @return note_id Id of the generated note.
    } {
        set note_id [db_string dup "select note_id
            from    im_notes
            where   object_id = :object_id and note = :note
        " -default ""]

        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }

        if {$note_id eq "" && $note ne ""} {

            if {$note_type_id eq ""} {set note_type_id [im_note_type_other]}
            
            set note_id [db_exec_plsql create_note "
                SELECT im_note__new(
                    NULL,
                    'im_note',
                    now(),
                    :user_id,
                    '[ad_conn peeraddr]',
                    null,
                    :note,
                    :object_id,
                    :note_type_id,
                    [im_note_status_active]
                )
            "]
        } 

        return $note_id
    }
}

ad_proc -public im_note_type_delivery_info {} {} {return 11515}
ad_proc -public im_note_type_project_info {} {} {return 11516}