
ad_library {
    APM callback procedures.

    @creation-date 2021-08-06
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::apm {}

ad_proc -public cog::apm::after_upgrade {
    {-from_version_name:required}
    {-to_version_name:required}
} {
    After upgrade callback.
} {
    apm_upgrade_logic \
        -from_version_name $from_version_name \
        -to_version_name $to_version_name \
        -spec {
            5.0.2.4.9 5.0.2.5.0 {
                # Remove richtext from notes.
                # Make it all html
                db_foreach notes {select note_id, note from im_notes} {
                    catch {set note [template::util::richtext::get_property html_value $note]}
                    regsub -all {\n} $note {<br />} note
                    if {$note eq ""} {
                        im_note_nuke $note_id
                    } else {
                        db_dml update_note "update im_notes set note = :note where note_id = :note_id"
                    }
                }
            }
        }
}

ad_proc -public cog::apm::after_install {} {
    Setup cognovis core
} {

    # Remove richtext from notes.
    # Make it all html
    db_foreach notes {select note_id, note from im_notes} {
        catch {set note [template::util::richtext::get_property html_value $note]}
        regsub -all {\n} $note {<br />} note
        if {$note eq ""} {
            im_note_nuke $note_id
        } else {
            db_dml update_note "update im_notes set note = :note where note_id = :note_id"
        }
    }

}
