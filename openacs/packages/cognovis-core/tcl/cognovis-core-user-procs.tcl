ad_library {
    cognovis specific procedures for users
    
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::user {
    ad_proc -public main_company_id {
        -user_id:required
    } {
        Return the main company id for given user_id. This should return single id or empty string 

        @param user_id User for whom we search the company
        @return company_id ID of the company
        
    } {
        set company_id [db_string company_id_sql "select company_id from im_companies where primary_contact_id =:user_id limit 1" -default ""]
        if {$company_id eq ""} {
            set company_id [db_string company_id_sql "select company_id from im_companies where accounting_contact_id =:user_id limit 1" -default ""]
        }
        if {$company_id eq ""} {
            set company_id [db_string company_id_sql "select object_id_one as company_id from acs_rels where object_id_two =:user_id and rel_type = 'im_company_employee_rel' limit 1" -default ""]
        }
        return $company_id
    }
}