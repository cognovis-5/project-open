<if @read_p@>
	@currency_outdated_warning;noquote@
 	<table>
   		<tr valign=top>
   			<td valign=top>
   				@cost_html;noquote@
    			<br />
  				@summary_html;noquote@
   			</td>
    		<td>
    			@admin_html;noquote@
    		</td>
    	</tr>
    	<tr>
    		<td colspan=2></td>
    	</tr>
    </table>
</if>
