<master>
<property name="title">@page_title@</property>
<property name="context">@context_bar@</property>
<property name="focus">@page_focus@</property>
<property name="admin_navbar_label">admin_categories</property>

@page_body;noquote@