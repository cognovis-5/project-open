ad_page_contract {
    Overview of packages and their translations

} {
    { limit_p 1}
} 

set elements [list]

lappend elements package_key {
    label "[_ acs-lang.Package]"
}

if {$limit_p} {
    set package_sql "select distinct package_key from lang_messages where package_key like 'webix%' 
        or package_key like 'sencha%' or package_key like 'cognovis%' or package_key like 'intranet-cust%' order by package_key"
} else {
    set package_sql "select distinct package_key from lang_messages order by package_key"
}
set locale_list [db_list locale_list_select {select locale from ad_locales where enabled_p = 'true' order by default_p}]

set extends [list]
foreach locale $locale_list {
    ds_comment "Locale $locale"
    lappend extends $locale
    lappend extends "${locale}_url"
    lappend extends "${locale}_upload_url"
    lappend extends "${locale}_download_url"
    lappend elements $locale
    switch $locale {
        de_DE {
            set flag "<span class='iconify' data-icon='emojione:flag-for-germany'></span>"
        }
        en_US {
            set flag "<span class='iconify' data-icon='emojione:flag-for-united-states'></span>"
        }
        es_ES {
            set flag "<span class='iconify' data-icon='emojione:flag-for-spain'></span>"
        }
        default {
            set flag $locale
        }
    }
    lappend elements [list label "$flag" display_template "
            <a href='@packages.${locale}_url@' title='View all messages in package'><i class='far fa-eye'></i></a>
            <a href='@packages.${locale}_download_url@' title='Download all messages in package for $locale'><i class='far fa-download'></i></a>"]
}



db_multirow -extend $extends packages packages $package_sql {
    foreach locale $locale_list {
        set ${locale}_url [export_vars -base "/acs-lang/admin/message-list" { locale package_key }]
        set ${locale}_upload_url [export_vars -base "upload-messages" { locale package_key }]
        set ${locale}_download_url [export_vars -base "download-messages" { locale package_key }]
    }
}

template::list::create \
    -name packages \
    -multirow packages \
    -elements $elements \

