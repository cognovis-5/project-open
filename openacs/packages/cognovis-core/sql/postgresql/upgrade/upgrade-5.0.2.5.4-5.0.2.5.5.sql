SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.5.4-5.0.2.5.5.sql','');
create or replace function inline_0 ()
returns integer as '
declare
        v_count         integer;
begin
        select count(*) into v_count from user_tab_columns
        where lower(table_name) = ''im_invoice_items'' and lower(column_name) = ''context_id'';
        if v_count > 0 then return 0; end if;

        alter table im_invoice_items add context_id integer
        constraint im_invoice_items_context_fk references acs_objects on delete set null;

        return 0;
end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();