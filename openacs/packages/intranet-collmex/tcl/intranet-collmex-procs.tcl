# packages/intranet-collmex/tcl/intranet-collmex-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Procedure to interact with collmex
    
    @author <yourname> (<your email>)
    @creation-date 2012-01-04
    @cvs-id $Id$
}

namespace eval intranet_collmex {
    ad_proc -public error {
        {-object_id ""}
        -message:required
    } {
        Throws an error which will notify the accounting_contact_id

        @param object_id Object which resulted in the error
        @param message Message with the error 
    } {
        set accounting_contact_id [db_string accounting "select accounting_contact_id from im_companies where company_path = 'internal'" -default [ad_host_administrator]]	

        set subject "Collmex Error" 
        if {$object_id ne ""} {
            lappend subject " for [im_name_from_id $object_id]"
        }
        set project_id [db_string project "select project_id from im_costs where cost_id = :object_id" -default ""]
        if {[intranet_chilkat::send_mail \
            -to_party_id $accounting_contact_id \
            -from_party_id $accounting_contact_id \
            -subject "$subject" \
            -body "There was a error in collmex, data is not transferred.<p /><br />$message" \
            -no_callback] eq ""} {
            cog_log Error "We could not send the mail due to mail server complications. Contact your sysadmin with the project_nr, freelance and current date/time"
        }
        if {[info commands "webix::notification::create"] ne ""} {
            set recipient_id [auth::get_user_id]
            webix::notifications::create -object_id $object_id -recipient_id $recipient_id -message "$subject: $message" -project_id $project_id
            webix::notifications::create -object_id $object_id -recipient_id $accounting_contact_id -message "$subject: $message" -project_id $project_id
        }
    }
}

ad_proc -public intranet_collmex::http_post {
    {-csv_data ""}
    {-object_id ""}
} {
    Post to collmex
} {
    # Make sure we can use HTTPS
    ::http::register https 443 ::tls::socket

    set customer_nr [parameter::get_from_package_key -package_key intranet-collmex -parameter CollmexKundenNr]
    set login [parameter::get_from_package_key -package_key intranet-collmex -parameter Login]
    set password  [parameter::get_from_package_key -package_key intranet-collmex -parameter Password]
    set active_p [parameter::get_from_package_key -package_key intranet-collmex -parameter ActiveP]

    set data "LOGIN;$login;$password\n${csv_data}\n"

    set meldungstyp ""

    if {$active_p} {
            
        # Save the content to a file.
        set tmp_path [ns_mktemp]
        set file [open $tmp_path w]
        fconfigure $file -encoding "iso8859-1"
        puts $file $data
        flush $file
        close $file

        set http [new_CkHttp]
        set reqBody [new_CkByteData]
        set fac [new_CkFileAccess]
        set success [CkFileAccess_ReadEntireFile $fac $tmp_path $reqBody]
        set reply [CkHttp_postBinary $http "https://www.collmex.de/cgi-bin/cgi.exe?$customer_nr,0,data_exchange" $reqBody "text/csv" 0 0]

        set status_code [CkHttp_get_LastStatus $http]
        file delete $tmp_path
       
        if {$status_code ne 200} {
            set meldungstyp "E"
            set response "$status_code ... $reply"
        } else {
            set response $reply
            set meldungstyp [lindex [split $response ";"] 1]
        }
        
        switch $meldungstyp {
            S {
                return $response
            }
            W {
                # Warning Mail
                intranet_collmex::error -object_id $object_id -message "There was a warning in collmex, but the call was successful. <p /> \
                        <br />Called: $csv_data \
                        <br />Reponse $response"
                return $response
            }
            E {
                if {[string match "INVOICE_PAYMENT_GET*" $csv_data]} {
                    # It is not a critical issue if we can't get the payments, therefore we only
                    # log the issue as a notice
                    cog_log Notice "Error in Collmex: $response when calling $csv_data"
                } else {
                    # Error Mail
                    intranet_collmex::error -object_id $object_id -message "There was a warning in collmex, but the call was successful. <p /> \
                        <br />Called: $csv_data \
                        <br />Reponse $response"
                }
                return $response
            }
            default {
                return $response
            }
        }
    } else {
        return "Collmex not active"
    }
}

ad_proc -public intranet_collmex::update_company {
    -company_id
    -customer:boolean
    -create:boolean
} {
    send the company to collmex for update

    use field description from http://www.collmex.de/cgi-bin/cgi.exe?1005,1,help,daten_importieren_kunde
} {
    
    set company_type_id [db_string company_type "select company_type_id from im_companies where company_id = :company_id" -default ""]
    set csv_line ""

    if {[im_category_is_a [im_company_type_customer] $company_type_id]} {
        set csv_line [intranet_collmex::CMXKND -customer_id $company_id]
    }

    if {[im_category_is_a [im_company_type_provider] $company_type_id]} {
        set csv_line [intranet_collmex::CMXLIF -provider_id $company_id]
    }

    if {$csv_line eq ""} {
        if {!$create_p} {
            # ignore this mail when we create the company using a callback.
            intranet_collmex::error -message "Company has no primary contact or primary contact has no E-Mail. Alternatively please check that the company has an address with a city. Therefore we can't create the company or any associated invoices in Collmex. Please fix this, go to the latest invoice document for the customer / provider and edit & save it. Then a transfer should work normally. If you still don't see it in collmex, please forward this E-Mail to your system administrator - $company_id" \
        }
        return 0
    }

    set response [intranet_collmex::http_post -csv_data $csv_line -object_id $company_id]
    if {$response != "-1"} {
        set response [split $response ";"]
        if {[lindex $response 0] == "NEW_OBJECT_ID"} {
            
            set collmex_id [lindex $response 1]
            # This seems to be a new customer
            db_dml update_collmex_id "update im_companies set collmex_id = $collmex_id where company_id = :company_id"
        }
    }
    return "$csv_line \n $response"
}

ad_proc -public intranet_collmex::update_provider_bill {
    -invoice_id
    -storno:boolean
} {
    send the provider bill to collmex
} {
    if {$storno_p} {
        set csv_line [intranet_collmex::CMXLRN -invoice_id $invoice_id -storno]
    } else {
        set csv_line [intranet_collmex::CMXLRN -invoice_id $invoice_id]
    }

    set response [intranet_collmex::http_post -csv_data $csv_line -object_id $invoice_id]
}

ad_proc -public intranet_collmex::update_customer_invoice {
    -invoice_id
    -storno:boolean
    -line_items:boolean
} {
    send the customer invoice to collmex
    
    @param invoice_id Invoice to be sent over to Collmex
    @param storno Add this flag if you want to storno the invoice
    @param line_items Add this flag if you want to transfer the individual lineitems. This only works with correctly maintained materials in the line items which link back to material groups that have a tax_id
} {
    set corr_invoice_nr ""
    
    # Get all the invoice information
    db_1row invoice_data {
        select collmex_id,to_char(effective_date,'YYYYMMDD') as invoice_date, invoice_nr, cost_type_id,
          round(vat,0) as vat, round(amount,2) as invoice_netto, c.company_id, address_country_code, ca.aux_int1 as customer_vat,
	  date_part('year',ci.delivery_date) - date_part('year',effective_date) as year_difference,
          ca.aux_int2 as customer_konto, cc.cost_center_code as kostenstelle, cb.aux_int2 as collmex_payment_term_id, amount
        from im_invoices i, 
	im_companies c, 
	im_offices o, 
	im_categories ca, 
	im_costs ci left outer join im_cost_centers cc on (cc.cost_center_id = ci.cost_center_id) left outer join im_categories cb on (cb.category_id = ci.payment_term_id)
	where c.company_id = ci.customer_id
	    and i.invoice_id = ci.cost_id 
	    and c.main_office_id = o.office_id
            and ca.category_id = c.vat_type_id
            and i.invoice_id = :invoice_id
    }

    if {$collmex_id eq ""} {
        set collmex_id [intranet_collmex::update_company -company_id $company_id -customer]
    }

    # In case we did not get line_items as a boolean, check if the invoice if of vat_type for line items.
    if {$line_items_p == 0} {set line_items_p [db_string line_item_p "select 1 from im_costs where vat_type_id = 42021 and cost_id = :invoice_id" -default 0]}

    set replacement_text ""
    set verrechnungs_gutschrift ""

    switch $cost_type_id {
	3725 {
	    set replacement_text "Korrigiert Rechnung"
	}
	3740 {
	    set replacement_text "Storniert Rechnung"
	}
    }

    if {$cost_type_id eq 3725 || $cost_type_id eq 3740} {
	
	# This is a correction invoice. we need to link it to the original invoice in wording
	set linked_invoice_ids [relation::get_objects -object_id_two $invoice_id -rel_type "im_invoice_invoice_rel"]
	
	if {$linked_invoice_ids ne ""} {
	    db_foreach linked_list "select c.cost_id as linked_invoice_id, cost_type_id as linked_cost_type_id,cost_status_id as linked_cost_status_id, invoice_nr as linked_invoice_nr
                from im_costs c, im_invoices i 
                where cost_id in ([template::util::tcl_to_sql_list $linked_invoice_ids])
                and cost_id = invoice_id" {
		# This is a credit note, let's apply it
		if {($linked_cost_type_id == [im_cost_type_invoice] || $linked_cost_type_id == [im_cost_type_correction_invoice])} {
		    append replacement_text "$linked_invoice_nr - "
		}
	    }
	    if {$cost_type_id eq 3740} {
		# Prüfen ob wir eine Zahlung haben für die Rechnung. Nur ohne Zahlung können wir Verrechnen
		if {![intranet_collmex::invoice_payment_get -invoice_id $linked_invoice_id -all]} {
		    set verrechnungs_gutschrift "$linked_invoice_nr"
		}
	    }
	}
    }

    # ---------------------------------------------------------------
    # Deal with previous year customer konto
    # This is a manual hack at the moment
    # ---------------------------------------------------------------

    if {$year_difference >0} {
	switch $customer_konto {
	    8400 {
		set customer_konto 1718
	    }
	}
    }

    if {$line_items_p} {
	cog_log Notice "Updating Collmex invoice for $invoice_id line items"
        db_1row item_data {select round(sum(item_units*price_per_unit),2) as total_amount, array_to_string(array_agg(item_name), ', ') as items_text
            from (select item_units,price_per_unit,item_name from im_invoice_items ii where ii.invoice_id = :invoice_id order by sort_order) as items}
        

        set csv_line ""
        # Transfer one FI line item per invoice line
        db_foreach line_item {
            select sum(round(item_units*price_per_unit,2)) as line_item_netto, ct.aux_int1 as vat, ct.aux_int2 as line_item_konto
            from im_categories cm, im_categories ct, im_invoice_items ii, im_materials im
            where cm.aux_int2 = ct.category_id
            and ii.item_material_id = im.material_id
            and im.material_type_id = cm.category_id
            and ii.invoice_id = :invoice_id
            group by ct.aux_int1,ct.aux_int2
        } {
            # Override line item vat if the customer is tax free.
            if {$customer_vat eq 0} {
                set vat 0
                set line_item_konto $customer_konto
            }
            
            regsub -all {\.} $line_item_netto {,} netto
            regsub -all {\-} $netto {} netto

            # Create one FI line item per sales order line item
            if {$csv_line ne ""} {append csv_line "\n"}
            append csv_line "CMXUMS"; # 1
            append csv_line ";$collmex_id" ; # 2 Lieferantennummer
            append csv_line ";1" ; # 3 Firma Nr
            append csv_line ";$invoice_date" ; # 4 Rechnungsdatum
            append csv_line ";$invoice_nr" ; # 5 Rechnungsnummer
            
            if {$vat eq 19 || $vat eq 16} {
                append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\"" ; # 6 Nettobetrag voller Umsatzsteuersatz
            } else {
            	append csv_line ";"
            }
            append csv_line ";" ; # 7 Steuer zum vollen Umsatzsteuersatz
            if {$vat eq 7} {
                append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\"" ;  # 8 Nettobetrag halber Umsatzsteuersatz
            } else {
            	append csv_line ";"
            }
            append csv_line ";" ; # 9 Steuer zum halben Umsatzsteuersatz
            append csv_line ";" ; # 10 Umsätze Innergemeinschaftliche Lieferung
            append csv_line ";" ; # 11 Umsätze Export
            if {$vat eq 0} {
                append csv_line ";$line_item_konto" ; # 12 Steuerfreie Erloese Konto
                append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\""; # Steuerfrei Betrag
            } else {
                append csv_line ";" ; # 12 Hat VAT => Nicht Steuerfrei
                append csv_line ";" ; # 13 Hat VAT => Nicht Steuerfrei
            }
            append csv_line ";\"EUR\"" ; # 14Währung (ISO-Codes)
            append csv_line ";" ; # 15 Gegenkonto
            if {$line_item_netto >=0} {
                append csv_line ";0" ; # 16 Rechnungsart
            } else {
                append csv_line ";1" ; # 16 Rechnungsart Gutschrift
            }
            append csv_line ";\"[im_csv_duplicate_double_quotes "$replacement_text $items_text"]\"" ; # 17 Belegtext
            append csv_line ";$collmex_payment_term_id" ; # 18 Zahlungsbedingung
            if {$vat eq 19 || $vat eq 16} {
            	append csv_line ";$line_item_konto" ; # 19 KontoNr voller Umsatzsteuersatz
            } else {
            	append csv_line ";" ; # 19 KontoNr voller Umsatzsteuersatz
            }
            if {$vat eq 7} {
            	append csv_line ";$line_item_konto" ; # 20 KontoNr halber Umsatzsteuersatz
            } else {
            	append csv_line ";" ; # 20 KontoNr halber Umsatzsteuersatz
            }
            append csv_line ";" ; # 21 reserviert
            append csv_line ";" ; # 22 reserviert
            if {$storno_p} {
                append csv_line ";1" ; # 23 Storno
            } else {
                append csv_line ";" ; # 23 Storno
            }
            append csv_line ";" ; # 24 Schlussrechnung
            append csv_line ";" ; # 25 Erloesart
            append csv_line ";\"projop\"" ; # 26 Systemname
            append csv_line ";\"$verrechnungs_gutschrift\"" ; # 27 Verrechnen mit Rechnugnsnummer fuer gutschrift
            append csv_line ";\"$kostenstelle\"" ; # 28 Kostenstelle
        }
    
    } else {
	cog_log Notice "Updating Collmex invoice for $invoice_id without line items"

        regsub -all {\.} $invoice_netto {,} netto
        regsub -all {\-} $netto {} netto

        set csv_line "CMXUMS"; # 1
	
        append csv_line ";$collmex_id" ; # 2 Lieferantennummer
        append csv_line ";1" ; # 3 Firma Nr
        append csv_line ";$invoice_date" ; # 4 Rechnungsdatum
        append csv_line ";$invoice_nr" ; # 5 Rechnungsnummer

        if {$customer_konto eq ""} {
            set konto [parameter::get_from_package_key -package_key "intranet-collmex" -parameter "KontoInvoice"]
        }

        # Find if the provide is from germany and has vat.
        if {$vat eq 19 || $vat eq 16} {

            append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\"" ; # 6 Nettobetrag voller Umsatzsteuersatz
        } else {
            append csv_line ";"
        }

        append csv_line ";" ; # 7 Steuer zum vollen Umsatzsteuersatz
        append csv_line ";" ; # 8 Nettobetrag halber Umsatzsteuersatz
        append csv_line ";" ; # 9 Steuer zum halben Umsatzsteuersatz
        append csv_line ";" ; # 10 Umsätze Innergemeinschaftliche Lieferung
        append csv_line ";" ; # 11 Umsätze Export
        if {$vat eq 19 || $vat eq 16} {
            append csv_line ";" ; # 12 Hat VAT => Nicht Steuerfrei
            append csv_line ";" ; # 13 Hat VAT => Nicht Steuerfrei
        } else {
            append csv_line ";$customer_konto" ; # 12 Steuerfreie Erloese Konto
            append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\""; # Steuerfrei Betrag
        }
        append csv_line ";\"EUR\"" ; # 14Währung (ISO-Codes)
        append csv_line ";" ; # 15 Gegenkonto
        if {$invoice_netto >=0} {
            append csv_line ";0" ; # 16 Rechnungsart
        } else {
            append csv_line ";1" ; # 16 Rechnungsart Gutschrift
        }
        append csv_line ";$replacement_text" ; # 17 Belegtext
        append csv_line ";$collmex_payment_term_id" ; # 18 Zahlungsbedingung
        if {$vat eq 19 || $vat eq 16} {
            append csv_line ";$customer_konto" ; # 19 KontoNr voller Umsatzsteuersatz
        } else {
            append csv_line ";"
        }
        append csv_line ";" ; # 20 KontoNr halber Umsatzsteuersatz
        append csv_line ";" ; # 21 reserviert
        append csv_line ";" ; # 22 reserviert
        if {$storno_p} {
            append csv_line ";1" ; # 23 Storno
        } else {
            append csv_line ";" ; # 23 Storno
        }
        append csv_line ";" ; # 24 Schlussrechnung
        append csv_line ";" ; # 25 Erloesart
        append csv_line ";\"projop\"" ; # 26 Systemname
	append csv_line ";\"$verrechnungs_gutschrift\"" ; # 27 Verrechnen mit Rechnugnsnummer fuer gutschrift
        append csv_line ";\"$kostenstelle\"" ; # 28 Kostenstelle
    }
    cog_log Notice "$csv_line"
    set response [intranet_collmex::http_post -csv_data $csv_line -object_id $invoice_id]
}
    
ad_proc -public intranet_collmex::invoice_payment_get {
    {-invoice_id ""}
    -all:boolean
} {
    get a list of invoice payments from collmex
} {
    
    set payment_id ""

    set csv_line "INVOICE_PAYMENT_GET;1"
    if {$invoice_id ne ""} {
        set invoice_nr [db_string invoice_nr "select invoice_nr from im_invoices where invoice_id = :invoice_id" -default ""]
        append csv_line ";${invoice_nr}"
    } else {
        append csv_line ";"
    }
    if {$all_p} {
        append csv_line ";"
    } else {
        append csv_line ";1"
    }
    append csv_line ";\"projop\"" ; # Systemname
    
    # Now get the lines from Collmex
    set lines [split [intranet_collmex::http_post -csv_data $csv_line] "\n"]

    cog_log Notice "Returned payments from Collmex: $lines"
    set return_html ""
    foreach line $lines {
        # Find out if it actually is a payment line
        set line_items [split $line ";"]
        if {[lindex $line_items 0] eq "INVOICE_PAYMENT"} {
            set collmex_id  "[lindex $line_items 5]-[lindex $line_items 6]-[lindex $line_items 7]"
            set date  [lindex $line_items 2] ; # Datum
            set amount  [lindex $line_items 4] ; # Actually paid amount
            set invoice_nr [lindex $line_items 1]
            regsub -all {,} $amount {.} amount
            # Check if we have this id already for a payment
	    set payment_id [db_string payment_id "select payment_id from im_payments where collmex_id = :collmex_id" -default ""]
            if {$payment_id ne ""} {
                db_dml update "update im_payments set received_date = to_date(:date,'YYYYMMDD'), amount = :amount where collmex_id = :collmex_id"
                append return_html "$invoice_nr <br> $amount :: $collmex_id"
            } else {
                # Find the invoice_id
                set invoice_id [db_string invoice_id "select invoice_id from im_invoices where invoice_nr = :invoice_nr" -default ""]
                if {$invoice_id ne "" && $collmex_id ne "--"} {
		    # Check if we received the payment already
		    set payment_id [db_string payment_id "select payment_id from im_payments where cost_id = :invoice_id and amount = :amount and received_date = to_date(:date,'YYYYMMDD')" -default ""]
		    
		    # Lets record the payment
		    if {$payment_id eq ""} {
			set payment_id [im_payment_create_payment -cost_id $invoice_id -actual_amount $amount -no_status_update]
			db_dml update "update im_payments set received_date = to_date(:date,'YYYYMMDD'), amount = :amount, collmex_id = :collmex_id where payment_id = :payment_id"
		    }
                }
            }
        }
    }
    if {$payment_id ne ""} {
	return 1
    } else {
	return 0
    }
}

ad_proc -public intranet_collmex::invoice_payment_info_get {
    {-payment_id ""}
    {-all_p 1}
} {
    get the payment booking
} {
    
    set collmex_payment_id [db_string payment "select collmex_id from im_payments where payment_id = :payment_id" -default ""]
    if {$collmex_payment_id ne ""} {
	set csv_line "ACCDOC_GET;1"
	set collmex_list [split $collmex_payment_id "-"]
	append csv_line ";[lindex $collmex_list 0];[lindex $collmex_list 1]"
	append csv_line ";" ; # Kontnummer
	append csv_line ";" ; # Kostenstelle
	append csv_line ";" ; # Kundennummber
	append csv_line ";" ; # Lieferant
	append csv_line ";" ; # Anlage
	append csv_line ";" ; # Rechnung
	append csv_line ";" ; # Reise
	append csv_line ";" ; # Text
	append csv_line ";" ; # Beleg von
	append csv_line ";" ; # Beleg bis
	append csv_line ";0" ; # Stornos
	if {$all_p} {
	    append csv_line ";"
	} else {
	    append csv_line ";1"
	}
	append csv_line ";\"projop\"" ; # Systemname
	append csv_line ";" ; # Zahlungsnummer

	# Now get the lines from Collmex
	set lines [split [intranet_collmex::http_post -csv_data $csv_line] "\n"]
	
	cog_log Notice "Returned payment info from Collmex: $lines"
	set note ""
	
	foreach line $lines {
	    # Find out if it actually is a payment line
	    set line_items [split $line ";"]
	    if {[lindex $line_items 0] eq "ACCDOC"} {
		set note [lindex $line_items 6] ; # Buchungstext
		db_dml update_note "update im_payments set note=:note where payment_id = :payment_id"
	    }
	}
	return $note
    } else {
	return 0
    }
}


ad_proc -public intranet_collmex::update_customer_invoices_by_nr {
    -invoice_nr_list
} {
    Resends a list of invoice numbers to collmex
    
    @param invoice_nr_list List of invoice_nr to resend to collmex
} {

    set invoice_ids [db_list invoice_ids "select invoice_id from im_invoices where invoice_nr in ([template::util::tcl_to_sql_list $invoice_nr_list ])"]
    
    foreach invoice_id $invoice_ids {
	    cog_log Notice "Creating invoice in Collmex:: [intranet_collmex::update_customer_invoice -invoice_id $invoice_id]"
    }
}

ad_proc -public intranet_collmex::open_items_get {
    {-company_id ""}
} {
    get a list of open items from collmex
} {
    
    set csv_line "OPEN_ITEMS_GET;1"
    append csv_line ";0" ; # Kunden (1 für lieferanten)

    if {$company_id ne ""} {
        set collmex_id [db_string invoice_nr "select collmex_id from im_companies where company_id = :company_id" -default ""]
        append csv_line ";${invoice_nr}"
    } else {
        append csv_line ";"
    }
    append csv_line ";" ; # Lieferantennummer
    append csv_line ";" ; # Vermittler


    # Now get the lines from Collmex
    set lines [split [intranet_collmex::http_post -csv_data $csv_line] "\n"]

    cog_log Notice "Returned payments from Collmex: $lines"
    set return_html "<table><tr><td>Kunde</td><td>Rechnung</td><td>Datum</td><td>Amount</td></tr>"
    foreach line $lines {
	
        # Find out if it actually is a payment line
        set line_items [split $line ";"]
        if {[lindex $line_items 0] eq "OPEN_ITEM"} {
            set collmex_id  "[lindex $line_items 2]-[lindex $line_items 3]-[lindex $line_items 4]"
	    set customer_name [lindex $line_items 6]
            set invoice_nr [lindex $line_items 9]
            set date  [lindex $line_items 10] ; # Datum
	    set amount [lindex $line_items 17]
	    set paid_amount [lindex $line_items 18]
	    set open_amount [lindex $line_items 19]
            regsub -all {,} $amount {.} amount
            regsub -all {,} $open_amount {.} open_amount
            regsub -all {,} $paid_amount {.} paid_amount

	    append return_html "<tr><td>$customer_name</td><td>$invoice_nr</td><td>$date</td><td>$open_amount ($amount - $paid_amount)</td></tr>"
	    # Check if the sum of the payments for the invoice equals the paid_amount
	    # If not, delete all payments and load them again as we had a rebooking

	    # Safe to a database?
	    
        }
    }
    return $return_html
}
