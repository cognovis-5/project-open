# /packages/sencha-freelance-translation/www/notify-url
ad_page_contract {
    Redirect to the object while recording the view
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2017-08-24
} -query {
    context_id:notnull
}

# Redriect
ad_returnredirect [im_biz_object_url $context_id]
