# packages/intranet-mail/tcl/intranet-mail-callback-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Callback procs
    
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-04-21
    @cvs-id $Id$
}


ad_proc -public -callback intranet-mail::logged_email {
    -log_id:required
} {
    Callback that is executed once an E-Mail is successfully imported (read: we have a log_id)
} -



# Callback for creating the IMAP folder as well as the Mail file
# folder

ad_proc -public -callback im_project_after_create -impl intranet-mail_create_folders {
    {-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
    Create the IMAP folder as well as mail folder in intranet-fs for this project.
    
} {
    intranet-mail::create_project_folders -project_id $object_id
}


# Callback to move the IMAP Folder around if we change the parent
# project_id

ad_proc -public -callback im_project_after_update -impl intranet-mail_rename_imap {
    {-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
    Move the imap folder to the new parent project
    
} {
    if {[imap::configured_p]} {
        set project_id $object_id
        set project_nr [db_string parent_id "select project_nr from im_projects where project_id=:project_id" -default ""]
    
        # Check if the IMAP folder exists (how ever this is possible)
        set imap_folder [intranet-mail::project_imap_folder -project_id $project_id -check_imap]
        if {$imap_folder eq ""} {
            # We need to move the folder.... yikes ....
            # First find out the old folder
            set old_imap_folder [imap::full_mbox_name -mailbox [imap::mailboxes -pattern $project_nr]]
    
            # Set the new folder
            set new_folder [intranet-mail::project_imap_folder -project_id $project_id]
            set new_imap_folder [imap::full_mbox_name -mailbox $new_folder]
            ns_log Notice "From $old_imap_folder to $new_imap_folder :: $new_folder"
            set session_id [imap::start_session]
            ns_imap m_rename $session_id $old_imap_folder $new_imap_folder
            imap::end_session -session_id $session_id
        }
    }
}


ad_proc -public -callback acs_mail_lite::send -impl intranet-mail_tracking {
    -package_id:required
    -message_id:required
    -from_addr:required
    -to_addr:required
    -body:required
    {-mime_type "text/plain"}
    {-subject}
    {-cc_addr}
    {-bcc_addr}
    {-file_ids}
    {-object_id}
    {-filesystem_files}
} {
    create a new entry in the mail tracking table
} {
    # Don't log if we don't have an object_id
    if {$object_id eq ""} {
        return
    }

    set sender_id [party::get_by_email -email $from_addr]

    # Deal with the recipients and find out the ids, so we can
    # correctly map them
    set recipients [concat $to_addr $cc_addr $bcc_addr]
    
    if {[llength $recipients]>0} {
	db_foreach party "select party_id, email from parties where email in ([template::util::tcl_to_sql_list $recipients])" {
	    set party($email) $party_id
	}
    
	# First the to_addr
	set to_addr_list [list]
	set recipient_ids [list]
	foreach email $to_addr {
	    # If we have a party_id we should link it up and not put it in
	    # the to_addr list
	    if {[info exists party($email)]} {
		lappend recipient_ids $party($email)
	    } else {
            lappend to_addr_list $email
	    }
	}
	
	# cc_addr
	set cc_addr_list [list]
	set cc_ids [list]
	foreach email $cc_addr {
	    if {[info exists party($email)]} {
		lappend cc_ids $party($email)
	    } else {
		lappend cc_addr_list $email
	    }
	}
	
	set bcc_addr_list [list]
	set bcc_ids [list]
	foreach email $bcc_addr {
	    # If we have a party_id we should link it up and not put it in
	    # the to_addr list
	    if {[info exists party($email)]} {
		lappend bcc_ids $party($email)
	    } else {
		lappend bcc_addr_list $email
	    }
	}
	
	set log_id [intranet-mail::log_add -package_id $package_id \
			-sender_id $sender_id \
			-from_addr $from_addr \
			-recipient_ids $recipient_ids \
			-cc_ids $cc_ids \
			-bcc_ids $bcc_ids \
			-to_addr $to_addr_list \
			-cc_addr $cc_addr_list \
			-bcc_addr $bcc_addr_list \
			-body $body \
			-subject $subject \
			-context_id $object_id \
			-message_id $message_id \
			-file_ids $file_ids \
			-filesystem_files $filesystem_files
                   ]
    }
}

ad_proc -public -callback fs::file_delete -impl intranet-mail_tracking {
    {-package_id:required}
    {-file_id:required}
} {
    Create a copy of the file and attach it to the mail-tracking entry, if the file is referenced
} {

    if {[db_string file_attached_p "select 1 from acs_mail_log_attachment_map where file_id = :file_id" -default 0]} {
	set package_id [apm_package_id_from_key intranet-mail]
	set new_file_id [fs::file_copy -file_id $file_id -target_folder_id $package_id]
	db_dml update_file "update acs_mail_log_attachment_map set file_id = :new_file_id where file_id = :file_id"
    }
}

ad_proc -public -callback im_trans_invoice_before_nuke -impl intranet-mail {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Move the deleted invoice from the mail context to the project or recipient
} {
    
    set project_id [db_string project_id "select project_id from im_costs where cost_id = :object_id" -default ""]
    if {$project_id ne ""} {
	db_dml update_context "update acs_mail_log set context_id = :project_id where context_id = :object_id"
    } 
    return
}    

ad_proc -public -callback im_invoice_before_nuke -impl intranet-mail {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Move the deleted invoice from the mail context to the project or recipient
} {
    
    set project_id [db_string project_id "select project_id from im_costs where cost_id = :object_id" -default ""]
    if {$project_id ne ""} {
	db_dml update_context "update acs_mail_log set context_id = :project_id where context_id = :object_id"
    } 
    return
}    
